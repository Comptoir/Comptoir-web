<?php

namespace App\Test\TestCase\View\Helper;

/* use Cake\Network\Request;
  use Cake\Network\Session;
  use Cake\ORM\TableRegistry; */

use App\View\Helper\ListsHelper;
use Cake\Core\Configure;
use Cake\Network\Request;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 *
 */
class ListsHelperTest extends TestCase
{

    public $softwaresList;
    public $expextedHtmlStrucure;
    public $usersList;
    public $reviewsList;
    public $screenshotsList;
    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->View = new View();
        $this->View->request = new Request([]);

        $this->Lists = new ListsHelper($this->View);

        $this->softwaresList = json_encode(
            [[
            "id" => 15,
            "softwarename" => 'i-delibRE',
            "logo_directory" => 'files/Softwares/15/photo/avatar',
            "photo" => 'Softwares_Logo_idelibre.png',
            "description" => 'Le projet i-delibRE est le porte-document nomade des élus pour le suivi des séances délibérantes de la collectivité.',
            ]],JSON_PRETTY_PRINT
        );

        $this->usersList = json_encode(
            [[
                'username' => 'Cake',
                'logo_directory' => '',
                'url' => 'url',
                'user_type_id' => 2,
                'description' => 'A description',
                'modified' => null,
                'role' => 'Admin',
                'password' => 'passwd',
                'digest_hash'=>'passwd',
                'email' => 'name1@adullact.org',
                'photo' => 'url'
            ]],JSON_PRETTY_PRINT
        );

        $this->reviewsList = json_encode(
        [[
            "id" => 18,
            "comment" => 'dfpojfà i',
            "title" => 'Hey ',
            "created" => '2016-08-09T14:07:39+00:00',
            "user_id" => 66,
            "software_id"=> 33,
            "evaluation" => 5,
            "modified" => '2016-08-09T14:07:39+00:00',
            "user" =>  [
                "id" => 66,
                "username "=> 'Mika',
                "logo_directory" => null,
                "photo "=> null,
                "description" => null,
                "user_type" => [
                     "id" => 2,
                    "name" => 'Administration',
                    "created" => '2016-04-08T16:03:58+00:00',
                    "modified" => '2016-04-15T09:21:37+00:00',
                    "cd" => 'Administration',
                ]
            ]
        ]],JSON_PRETTY_PRINT
        );


        $this->screenshotsList = json_encode(
            [[
                "id" => 295,
                "software_id" => 33,
                "url_directory" => 'files/Screenshots',
                "created "=> '2016-06-15T13:18:43+00:00',
                "modified" => '2016-06-15T13:18:43+00:00',
                "name" => 'Screenshot_office_writerwindows.png',
                "photo" => '1465996723_Screenshot_office_writerwindows.png',
            ]]
            ,JSON_PRETTY_PRINT
        );

    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->Lists, $this->View);
    }

//    /**
//     * Tests the className method
//     *
//     * @covers App\View\Helper\ListsHelper::softwares
//     */
//    public function testSoftwares()
//    {
//        // If the list of softwares is empty
//        $this->assertEquals(null, $this->Lists->softwares([]));
//
//        $softwareItem = [
//            ["li" => ["class"=>"col-xs-12 col-sm-4 col-md-3 col-lg-2"]],
//
//                "div" => [ "class"=>"software-unit-all backgroundUnit "],
//                ["div" => [ "class"=>" score stamp score_NA"]],
//                    "abbr" => ["title"=>"Score : NA %"],"NA","/abbr",
//                "/div",
//                "h3"=> true,
//                    ['a' => ["href"]],'i-delibRE','/a',
//                '/h3'
//                ,
//                ['div'=>true],
//                    'a' => ["href","title"=>'i-delibRE'],
//                    'img'=>["src"=>"http://localhost/comptoir-srv/files/Softwares/15/photo/avatar/Softwares_Logo_idelibre.png", "alt"=>"i-delibRE", "class"=>"img-responsive size-logo"],
//                    '/a',
//                '                        </div>',
//                "p" => ["class"=>"text-overflow project-description"],"Le projet i-delibRE est le porte-document nomade des élus pour le suivi des séances délibérantes..."
//        ];
//
//        $this->assertHtml($softwareItem,$this->Lists->softwares(json_decode( $this->softwaresList) ));
//    }
//
//    /**
//     * Tests the className method
//     *
//     * @covers App\View\Helper\ListsHelper::users
//     */
//    public function testUsers()
//    {
//        // If the list of users is empty
//        $this->assertEquals(null, $this->Lists->users([]));
//
//        $userItem = [
//            ["li" => ["class"=>"col-xs-12 col-sm-4 col-md-3 col-lg-2"]],
//            ["div" => [ "class"=>"user-unit-all backgroundUnit list-unstyled"]],
//            ["div" => [ "class"=>" stamp badge badgeAsso"]],
//            "div" => ["class"=>"fa fa-users fa-2x"],
//            "span"=> ["class"=>"sr-only"],"/span",
//            '                        </div>',
//            "/div" => true,
//            "h3"=> ["class"=>"size-title"],
//            ['a' => ["href","title"=>"Cake"]],'Cake','/a',
//            '/h3'
//            ,
//            ['div'=>true],
//            'a' => ["href"],
//            'img'=>["src"=>"img/logos/User_placeholder.jpg", "alt"=>"Cake", "class"=>"img-responsive size-logo"],
//            '/a',
//            '                        </div>',
//        ];
//
//        $this->assertHtml($userItem,$this->Lists->users(json_decode( $this->usersList) ),true);
//    }
//
//    /**
//     * Tests the className method
//     *
//     * @covers App\View\Helper\ListsHelper::reviews
//     */
//    public function testReviews()
//    {
//
//        // If the list of reviews is empty
//        $this->assertEquals(null, $this->Lists->reviews([]));
//        $reviewItem = [
//            ["li" => ["class"=>"col-xs-11 col-sm-12 col-md-6 col-lg-6"]],
//            ["div" => [ "class"=>"blockReview"]],
//            ["div" => [ "class"=>"row"]],
//            "div" => ["class"=>"col-xs-3"],
//            ["p" => ["class"=>"UserReview"]],
//                "a"=> ["href"],"/a",
//            "/p",
//
//            //Evaluation 5 stats
//            ["p" => true],
//            ["span" => ["class"=>"glyphicon glyphicon-star"]],
//            ["span"=> ["class"=>"sr-only"]],'*','/span','/span',
//            ["span" => ["class"=>"glyphicon glyphicon-star"]],
//            ["span"=> ["class"=>"sr-only"]],'*','/span','/span',
//            ["span" => ["class"=>"glyphicon glyphicon-star"]],
//            ["span"=> ["class"=>"sr-only"]],'*','/span','/span',
//            ["span" => ["class"=>"glyphicon glyphicon-star"]],
//            ["span"=> ["class"=>"sr-only"]],'*','/span','/span',
//            ["span" => ["class"=>"glyphicon glyphicon-star"]],
//            ["span"=> ["class"=>"sr-only"]],'*','/span','/span',
//            ' 5',
//            "/p",
//            // END evaluation
//
//            ["p" => true,],"09/08/2016","/p",
//            '                                </div>',
//            ["div"=>["class"=>"col-xs-9"]],
//
//            "h3"=>true,"Hey ","/h3",
//            ["p" => true,],"dfpojfà i","/p",
//        ];
//
//        $this->assertHtml($reviewItem,$this->Lists->reviews(json_decode( $this->reviewsList) ));
//    }
//
//    /**
//     * Tests the className method
//     *
//     * @covers App\View\Helper\ListsHelper::screenshots
//     */
//    public function testScreenshots()
//    {
//
//        // If the list of screenshots is empty
//        $this->assertEquals(null, $this->Lists->screenshots([]));
//
//        $screenshotItem = [
//            ["li" => ["class"=>" col-xs-12 col-sm-6 col-md-3 col-lg-3 "]],
//            ["div" => [ "class"=>"size-title screenShot"]],
//            "a"=> ["href"],
//            "img"=> ["src"=>"http://localhost/comptoir-srv/files/Screenshots/1465996723_Screenshot_office_writerwindows.png", "alt"=>"", "class"=>"img-responsive "],
//            "/a",
//        ];
//
//        $this->assertHtml($screenshotItem,$this->Lists->screenshots(json_decode( $this->screenshotsList) ),true);
//    }

    /**
     * Tests the itemsName method
     *
     * @covers App\View\Helper\ListsHelper::items
     */
    public function testItems()
    {

        // If the list of screenshots is empty
        $this->assertEquals(null, $this->Lists->items([]));

        $screenshotItem = [
            ["ol"=> ["class"=>"row list-unstyled"]],
            ["li" => ["class"=>" col-xs-12 col-sm-6 col-md-3 col-lg-3 "]],
            ["div" => [ "class"=>"size-title screenShot"]],
            "a"=> ["href"],
            "img"=> ["src"=>"http://localhost/comptoir-srv/files/Screenshots/1465996723_Screenshot_office_writerwindows.png", "alt"=>"", "class"=>"img-responsive "],
            "/a",
        ];

        $reviewItem = [
            ["ol"=>["class"=>"row list-unstyled"]],
            ["li" => ["class"=>"col-xs-11 col-sm-12 col-md-6 col-lg-6"]],
            ["div" => [ "class"=>"blockReview"]],
            ["div" => [ "class"=>"row"]],
            "div" => ["class"=>"col-xs-3"],
            ["p" => ["class"=>"UserReview"]],
            "a"=> ["href"],"/a",
            "/p",

            //Evaluation 5 stats
            ["p" => true],
            ["span" => ["class"=>"glyphicon glyphicon-star"]],
            ["span"=> ["class"=>"sr-only"]],'*','/span','/span',
            ["span" => ["class"=>"glyphicon glyphicon-star"]],
            ["span"=> ["class"=>"sr-only"]],'*','/span','/span',
            ["span" => ["class"=>"glyphicon glyphicon-star"]],
            ["span"=> ["class"=>"sr-only"]],'*','/span','/span',
            ["span" => ["class"=>"glyphicon glyphicon-star"]],
            ["span"=> ["class"=>"sr-only"]],'*','/span','/span',
            ["span" => ["class"=>"glyphicon glyphicon-star"]],
            ["span"=> ["class"=>"sr-only"]],'*','/span','/span',
            ' 5',
            "/p",
            // END evaluation

            ["p" => true,],"09/08/2016","/p",
            '                                </div>',
            ["div"=>["class"=>"col-xs-9"]],

            "h3"=>true,"Hey ","/h3",
            ["p" => true,],"dfpojfà i","/p",
        ];

        $userItem = [
            ["ol"=>["class"=>"row list-unstyled"]],
            ["li" => ["class"=>"col-xs-12 col-sm-6 col-md-3 col-lg-3"]],
            ["div" => [ "class"=>"user-unit-home backgroundUnit list-unstyled"]],
            ["div" => [ "class"=>" stamp badge badgeAsso"]],
            "div" => ["class"=>"fa fa-users fa-2x"],
            "span"=> ["class"=>"sr-only"],"/span",
            '                        </div>',
            "/div" => true,
            "h3"=> ["class"=>"size-title"],
            ['a' => ["href","title"=>"Cake"]],'Cake','/a',
            '/h3'
            ,
            ['div'=>true],
            'a' => ["href"],
            'img'=>["src"=>"img/logos/User_placeholder.jpg", "alt"=>"Cake", "class"=>"img-responsive size-logo"],
            '/a',
            '                        </div>',
        ];

        $softwareItem = [
            ["ol"=>["class"=>"row list-unstyled"]],
            ["li" => ["class"=>"col-xs-12 col-sm-6 col-md-3 col-lg-3"]],

            "div" => [ "class"=>"software-unit-home backgroundUnit"],
            ["div" => [ "class"=>" score stamp score_NA"]],
            "abbr" => ["title"=>"Score : NA %"],"NA","/abbr",
            "/div",
            "h3"=> true,
            ['a' => ["href"]],'i-delibRE','/a',
            '/h3'
            ,
            ['div'=>true],
            'a' => ["href","title"=>'i-delibRE'],
            'img'=>["src"=>"http://localhost/comptoir-srv/files/Softwares/15/photo/avatar/Softwares_Logo_idelibre.png", "alt"=>"i-delibRE", "class"=>"img-responsive size-logo"],
            '/a',
            '                        </div>',
            "p" => ["class"=>"text-overflow project-description"],"Le projet i-delibRE est le porte-document nomade des élus pour le suivi des séances délibérantes..."
        ];

        $this->assertHtml($screenshotItem,$this->Lists->items(json_decode( $this->screenshotsList),"screenshots"));
        $this->assertHtml($userItem,$this->Lists->items(json_decode( $this->usersList),"users"),true);
        $this->assertHtml($softwareItem,$this->Lists->items(json_decode( $this->softwaresList),"softwares"));
        $this->assertHtml($reviewItem,$this->Lists->items(json_decode( $this->reviewsList),"reviews"));
    }



}
