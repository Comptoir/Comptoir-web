<?php

namespace App\Test\TestCase\View\Helper;

/* use Cake\Network\Request;
  use Cake\Network\Session;
  use Cake\ORM\TableRegistry; */

use App\View\Helper\ScoreHelper;
use Cake\Core\Configure;
use Cake\Network\Request;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 *
 */
class ScoreHelperTest extends TestCase {

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();

        $this->View = new View();
        $this->View->request = new Request([]);

        $this->Score = new ScoreHelper($this->View);

        Configure::write("LOW_MARK", 25);
        Configure::write("MEDIUM_MARK", 50);
        Configure::write("HIGH_MARK", 75);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown() {
        parent::tearDown();
        unset($this->Score, $this->View);
    }

    /**
     * Tests the className method around the threshold values
     *
     * @covers App\View\Helper\ScoreHelper::className
     */
    public function testClassNameThresholds() {
        $thresholds = [
            'score_low' => 25,
            'score_medium' => 50,
            'score_high' => 75,
            'score_best' => 100
        ];
        $this->assertEquals('score_low', $this->Score->className(24.4,$thresholds));

        $this->assertEquals('score_medium', $this->Score->className(24.6,$thresholds));
    }

    /**
     * Tests the className method
     *
     * @covers App\View\Helper\ScoreHelper::className
     */
    public function testClassName() {
        $thresholds = [
            'score_low' => 25,
            'score_medium' => 50,
            'score_high' => 75,
            'score_best' => 100
        ];

        $this->assertEquals('score_low', $this->Score->className(0, $thresholds));
        $this->assertEquals('score_low', $this->Score->className(24, $thresholds));

        $this->assertEquals('score_medium', $this->Score->className(24.6, $thresholds));
        $this->assertEquals('score_medium', $this->Score->className(25, $thresholds));
        $this->assertEquals('score_medium', $this->Score->className(26, $thresholds));
        $this->assertEquals('score_medium', $this->Score->className(26.3, $thresholds));

        $this->assertEquals('score_high', $this->Score->className(51, $thresholds));

        $this->assertEquals('score_best', $this->Score->className(100, $thresholds));
    }

    public function testNote() {
        // Test to see if note2 with no parameters returns the standard score badge.
        $this->assertHtml(["span"=>["class"=>"score score_low"],"0"],$this->Score->note(),true);
        // Test to see if note2 with $score as a parameter returns the correct score badge.
        $this->assertHtml(["span"=>["class"=>"score score_low"],"0"],$this->Score->note(0),true);
        $this->assertHtml(["span"=>["class"=>"score score_low"],"24"],$this->Score->note(24),true);
        $this->assertHtml(["span"=>["class"=>"score score_medium"],"25"],$this->Score->note(25),true);
        $this->assertHtml(["span"=>["class"=>"score score_medium"],"49"],$this->Score->note(49),true);
        $this->assertHtml(["span"=>["class"=>"score score_high"],"50"],$this->Score->note(50),true);
        $this->assertHtml(["span"=>["class"=>"score score_high"],"74"],$this->Score->note(74),true);
        $this->assertHtml(["span"=>["class"=>"score score_best"],"75"],$this->Score->note(75),true);
        $this->assertHtml(["span"=>["class"=>"score score_best"],"100"],$this->Score->note(100),true);
        // Test to see if note2 rounds up correctly and displays the correct badge as well.
        $this->assertHtml(["span"=>["class"=>"score score_high"],"50"],$this->Score->note(49.6),true);
        // Test with configs to see if the badge is created correctly
        $this->assertHtml(["span"=>["class"=>"score"],"50"],$this->Score->note(50,['class'=>"score"]),true);
        $this->assertHtml(["div"=>["class"=>"score"],"50"],$this->Score->note(50,['class'=>"score",'tag'=>'div']),true);
        $this->assertHtml(["div"=>["class"=>"score"],"50"],$this->Score->note(50,['class'=>"score",'tag'=>'div','template'=> 'score_default']),true);
        // Test with config template set to 'raw'
        $this->assertHtml(["50"],$this->Score->note(50,['class'=>"score",'tag'=>'div','template'=> 'score_raw']),true);
    }

    /**
     * Tests the testPrefix method
     *
     * @covers App\View\Helper\ScoreHelper::prefix
     */
    public function testPrefix() {
        // Test to check all config options
        $this->assertHtml(["span"=>["class"=>""]],$this->Score->prefix(),true);
        $this->assertHtml(["span"=>["class"=>""],"Score : "],$this->Score->prefix(['text' => "Score : "]),true);
        $this->assertHtml(["span"=>["class"=>""],"Score : "],$this->Score->prefix(['tag'=>"span",'text' => "Score : "]),true);
        $this->assertHtml(["div"=>["class"=>""],"Score : "],$this->Score->prefix(['tag'=>"div",'text' => "Score : "]),true);
        $this->assertHtml(["span"=>["class"=>""],"Score : "],$this->Score->prefix(['tag'=>"span",'text' => "Score : ",'template'=>"prefix"]),true);
        $this->assertHtml(["span"=>["class"=>"score score_low"],"Score : "],$this->Score->prefix(['tag'=>"span",'text' => "Score : ",'template'=>"prefix",'class'=>"score score_low"]),true);
        // Test for raw output
        $this->assertHtml(["Score : "],$this->Score->prefix(['template'=>"prefix_raw",'tag'=>"span",'text' => "Score : ",'class'=>"score score_low"]),true);
    }

    /**
     * Tests the testSuffix method
     *
     * @covers App\View\Helper\ScoreHelper::suffix
     */
    public function testSuffix() {
        // Test to check all config options
        $this->assertHtml(["span"=>["class"=>""]],$this->Score->suffix(),true);
        $this->assertHtml(["span"=>["class"=>""],"Score : "],$this->Score->suffix(['text' => "Score : "]),true);
        $this->assertHtml(["span"=>["class"=>""],"Score : "],$this->Score->suffix(['tag'=>"span",'text' => "Score : "]),true);
        $this->assertHtml(["div"=>["class"=>""],"Score : "],$this->Score->suffix(['tag'=>"div",'text' => "Score : "]),true);
        $this->assertHtml(["span"=>["class"=>""],"Score : "],$this->Score->suffix(['tag'=>"span",'text' => "Score : ",'template'=>"prefix"]),true);
        $this->assertHtml(["span"=>["class"=>"score score_low"],"Score : "],$this->Score->suffix(['tag'=>"span",'text' => "Score : ",'template'=>"suffix",'class'=>"score score_low"]),true);
        // Test for raw output
        $this->assertHtml(["Score : "],$this->Score->suffix(['template'=>"suffix_raw",'tag'=>"span",'text' => "Score : ",'class'=>"score score_low"]),true);
    }
}
