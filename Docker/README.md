# MANIVELLE Docker instructions

## Build

From the directory containing the `Dockerfile`:

```sh
docker stop manivelle-web ; docker rmi manivelle-web ; docker build -t manivelle-web .
```

## Run

```sh
docker run -it -p 81:80 --rm --name manivelle-web manivelle-web 
```

# Get into the running container

```sh
docker exec -it manivelle-web /bin/bash
```