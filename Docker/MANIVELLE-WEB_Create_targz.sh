#!/bin/bash

MyTag="Sprint008"
OldPwd="$(pwd)"
TGZ_Name="Manivelle-web-${MyTag}.tar.gz"

cd /tmp
rm -rf manivelle-web Manivelle-web-${MyTag}.tar.gz
git clone --branch "${MyTag}" https://adullact.net/anonscm/git/manivelle-web/manivelle-web.git 
cd manivelle-web
tar cvfz $TGZ_Name  \
    ./bin/ \
    ./config/ \
    ./plugins/ \
    ./src/ \
    ./webroot \
    ./index.php \
    ./composer.json \
    ./composer.lock \
    ./.htaccess

mv $TGZ_Name $OldPwd
rm -rf manivelle-web/
cd $OldPwd