# Se retirer de la liste fournisseur de service

# 1. Se connecter

* Cliquer sur "Connexion"
* Remplir les champs
    * Identifiant => [alias]-presta@comptoir-du-libre.org
    * Mot de passe => Test-MyCompany
    
## 2. Se d'écalerer comme fournisseur de service sur Asqatasun

* Se positionner sur la page du logiciel Asquatasun
* Cliquer sur **Se déclarer fournisseur de services**
* Vérifier la présence du message de validation.
* Vérifier que l'utilisateur **Test-MyConpany** apparaisse dans la rubrique **Fournisseurs de services de Asqatasun** sur la page de logiciel Asqatasun
* Si plus de 4 fournisseurs de services : 
    * Cliquer sur le lien **Voir tous**
    * Vérifier que l'utilsateur apparaisse dans la liste **Fournisseurs de services de Asqatasun**
    
## 3. Se retirer de la liste des fournisseurs de services de Asqatasun (Moins de 4 fournisseurs de services pour Asqatasun) 

* Se positionner sur la page du logiciel Asquatasun
* Vérifier que l'utilsateur apparaisse dans la liste **Fournisseurs de services  de Asqatasun**
* Cliquer sur **Ne plus être fournisseur de service**
* Vérifier la présence du message de validation.
* Vérifier que l'utilisateur **Test-MyCompany** n'apparaisse plus dans la liste des fournisseurs de services d'Asqatasun

## 4. Se d'écalerer comme fournisseur de service sur Asqatasun

* Se positionner sur la page du logiciel Asquatasun
* Cliquer sur **Se fournisseur de services**
* Vérifier la présence du message de validation.
* Vérifier que l'utilisateur **Test-MyConpany** apparaisse dans la rubrique **Fournisseurs de services de Asqatasun** sur la page de logiciel Asqatasun
* Si plus de 4 fournisseurs de services : 
    * Cliquer sur le lien **Voir tous**
    * Vérifier que l'utilsateur apparaisse dans la liste **Fournisseurs de services de Asqatasun**
* Revenir sur la page de Asqatasun

## 5. Se retirer de la liste des fournisseurs de services de Asqatasun (Plus de 4 fournisseurs de services pour Asqatasun) 

* Se positionner sur la page du logiciel Asquatasun
* Cliquer sur le lien **Voir tous**
* Vérifier que l'utilsateur apparaisse dans la liste **Fournisseurs de services de Asqatasun**
* Cliquer sur **Ne plus être fournisseur de service**
* Vérifier la présence du message de validation.
* Vérifier que l'utilisateur **Test-MyCompany** n'apparaisse plus dans la liste des fournisseurs de services d'Asqatasun



