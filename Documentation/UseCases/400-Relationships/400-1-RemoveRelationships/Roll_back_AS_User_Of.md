# Se retirer de la liste utilisateur

# 1. Se connecter

* Cliquer sur "Connexion"
* Remplir les champs
    * Identifiant => [alias]-collectivite@comptoir-du-libre.org
    * Mot de passe => TEST-Administration
    
## 2. Se d'écalerer comme utilisateur sur Asqatasun

* Se positionner sur la page du logiciel Asquatasun
* Cliquer sur **Se déclarer utilisateur**
* Vérifier la présence du message de validation.
* Vérifier que l'utilisateur **TEST-Administration** apparaisse dans la rubrique **Utilisateur de Asqatasun** sur la page de logiciel Asqatasun
    
## 3. Se retirer de la liste des utilisateurs de Asqatasun

* Se positionner sur la page du logiciel Asquatasun
* Cliquer sur **Ne plus être fournisseur de service**
* Vérifier la présence du message de validation.
* Vérifier que l'utilisateur **TEST-Administration** n'apparaisse plus dans la liste des utilisateurs d'Asqatasun