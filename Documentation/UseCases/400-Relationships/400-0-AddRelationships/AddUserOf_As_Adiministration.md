# Se déclarer en tant qu'utilisateur d'un logiciel (Asqatasun)

# 1. Se connecter

* Cliquer sur "Connexion"
* Remplir les champs
    * Identifiant => [alias]-collectivite@comptoir-du-libre.org
    * Mot de passe => TEST-Administration
    
## 2. Se d'écalerer comme utilisateur d'un logiciel

* Se positionner sur la page du logiciel Asquatasun
* Cliquer sur **Se déclarer utilisateur**
* Vérifier la présence du message de validation.
* Vérifier que l'utilisateur **TEST-Administration** apparaisse dans la rubrique **Utilisateur d'Asqatasun** sur la page de logiciel Asqatasun
    