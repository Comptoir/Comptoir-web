# Se déclarer en tant que fournisseur de services

## Première déclaration

* Se connecter sur le comptoir du libre
    * Identifiant => [alias]-presta@comptoir-du-libre.org
    * Mot de passe => Test-MyCompany 
* Se positionner sur la page d'un logiciel -> Asquatasun
* Cliquer sur le bouton "Se déclarer fournisseur de service"
* Vérifier la présence du message de validation.
* Vérifier que l'utilisateur **Test-MyConpany** apparaisse dans la rubrique **Fournisseur de services de Asqatasun** sur la page de logiciel Asqatasun


## Déclaration double

* Se connecter sur le comptoir du libre
    * Identifiant => [alias]-presta@comptoir-du-libre.org
    * Mot de passe => Test-MyCompany
* Se positionner sur la page du même logiciel que précédement -> Asquatasun
* Le bouton "Se déclarer fournisseur de service" ne doit pas apparaître 
* Le bouton "Ne plus être fournisseur de service" doit apparaître