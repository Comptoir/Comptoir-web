# Se déclarer en tant qu'utilisateur d'un logiciel (Asqatasun)

# 1. Se connecter

* Cliquer sur "Connexion"
* Remplir les champs
    * Identifiant => [alias]-individu@comptoir-du-libre.org
    * Mot de passe => Test-individu
    
## 2. Se d'écalerer comme utilisateur d'un logiciel

* Se positionner sur la page du logiciel Asquatasun
* Cliquer sur **Se déclarer utilisateur**
* Vérifier la présence du message de validation.
* Vérifier que l'utilisateur **Test-individu** apparaisse dans la rubrique **Utilisateur d'Asqatasun** sur la page de logiciel Asqatasun
    