# Se déclarer en tant qu'utilisateur d'un logiciel (Asqatasun)

# 1. Se connecter

* Cliquer sur "Connexion"
* Remplir les champs
    * Identifiant => [alias]-presta@comptoir-du-libre.org
    * Mot de passe => Test-MyCompany
    
## 2. Se d'écalerer comme utilisateur d'un logiciel

* Se positionner sur la page du logiciel Asquatasun
* Le bouton "Se déclarer utilisateur" ne doit pas apparaître    