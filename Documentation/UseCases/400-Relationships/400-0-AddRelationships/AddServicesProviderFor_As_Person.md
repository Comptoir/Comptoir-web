# Se déclarer en tant que fournisseur de service

# 1. Se connecter

* Cliquer sur "Se connecter"
* Remplir les champs
    * Identifiant => [alias]-individu@comptoir-du-libre.org
    * Mot de passe => Test-individu
    
## 2. Se d'écalerer comme fournisseur de service sur Asqatasun

* Se positionner sur la page du logiciel Asquatasun
* Cliquer sur **Se déclarer en tant que fournisseur**
* Vérifier la présence du message de validation.
* Vérifier que l'utilisateur **Test-individu** apparaisse dans la rubrique **Fournisseur de services de Asqatasun** sur la page de logiciel Asqatasun
    