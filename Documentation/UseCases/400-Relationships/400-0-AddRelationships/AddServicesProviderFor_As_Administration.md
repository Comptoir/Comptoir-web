# Se déclarer en tant que fournisseur de services

## Première déclaration

* Se connecter sur le comptoir du libre
    * Identifiant => [alias]-collectivite@comptoir-du-libre.org
    * Mot de passe => TEST-Administration 
* Se positionner sur la page d'un logiciel -> Asquatasun
* Cliquer sur le bouton "Se déclarer fournisseur de service"
* Vérifier la présence du message de validation.

## Déclaration double

* Se connecter sur le comptoir du libre
    * Identifiant => [alias]-collectivite@comptoir-du-libre.org
    * Mot de passe => TEST-Administration 
* Se positionner sur la page du même logiciel que précédement -> Asquatasun
* Le bouton "Se déclarer fournisseur de service" ne doit pas apparaître 
* Le bouton "Ne plus être fournisseur de service" doit apparaître
