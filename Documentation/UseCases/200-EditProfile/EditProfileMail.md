# Modifier le mail

## 0. Se connecter

* Cliquer sur "Se connecter"
* Remplir les champs
    * Identifiant => **[alias]-collectivite@comptoir-du-libre.org**
    * Mot de passe => **TEST-Administration**
* Valider le formulaire

## 1. Edition du mail profile

* Dans le menu utilisateur cliquer sur **Profil**
* Modifier les informations : 
    * Courriel : **[alias]-collectivite-new@comptoir-du-libre.org**
* Valider le formulaire

## 2. Deconnexion/Connexion

* Cliquer sur déconnexion
* Cliquer sur "Se connecter"
* Dans le menu utilisateur cliquer sur **Profil**
* Remplir les champs avec **[alias]-collectivite@comptoir-du-libre.org**
    * Un message d'erreur de type "login mot de passe incorect" doit appaître
* Remplir les champs avec **[alias]-collectivite-new@comptoir-du-libre.org**.
    * Un message de confirmation de connexion doit apparaître.

## 3. Edition du mail profile

* Dans le menu utilisateur cliquer sur **Profil**
* Modifier les informations : 
    * Courriel : **[alias]-collectivite@comptoir-du-libre.org**
* Valider le formulaire