# Editer le nom du profil

## 0.Se connecter

* Cliquer sur "Se connecter"
* Remplir les champs
    * Identifiant => **[alias]-collectivite@comptoir-du-libre.org**
    * Mot de passe => **TEST-Administration**
* Valider le formulaire

## 1.Edition du nom

* Dans le menu utilisateur cliquer sur **Profil**
* Modifier les informations : 
    * Nom : TEST-Administration-modifier
* Valider le formulaire


## 2.Vérification de la modification

* Cliquer sur déconnexion
* Se connecter => cf [Se connecter](##0.Se connecter)
* Vérifier que le nom afficher soit bien **TEST-Administration-modifier**

## 3.Edition du nom

* Dans le menu utilisateur cliquer sur **Profil**
* Modifier les informations : 
    * Nom : TEST-Administration
* Valider le formulaire

