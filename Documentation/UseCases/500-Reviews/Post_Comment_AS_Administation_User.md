# Poster un commentaire

## 1. Poster un commentaire 

* Cliquer sur "Se connecter"
* Remplir les champs
    * Identifiant => [alias]-collectivite@comptoir-du-libre.org
    * Mot de passe => TEST-Administration
* Valider le formulaire
* Aller sur la page d'un logiciel (ex: Asqatasun, LibreOffice)
* Cliquer sur ajouter un commentaire
* Remplir les champs obligatoires (signalés par **"*"** rouge)
    * Titre
    * Commentaire
    * Evaluations
* Vérifier la présence du message de confirmation
* Vérifier la redirection sur la page du logiciel commenté
* Vérifier que le commentaire apparaisse sur la page du logiciel
