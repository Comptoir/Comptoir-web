# Poster un commentaire

## 1. Poster un commentaire => Société

* Cliquer sur "Se connecter"
* Remplir les champs
    * Identifiant => [alias]-presta@comptoir-du-libre.org
    * Mot de passe => Test-MyCompany
* Valider le formulaire
* Aller sur la page d'un logiciel (ex: Asqatasun, LibreOffice)
* Le bouton "Témoigner" ne doit pas appraître   

## 2. Poster un commentaire => Individu

* Cliquer sur "Se connecter"
* Remplir les champs
    * Identifiant => [alias]-individu@comptoir-du-libre.org
    * Mot de passe => Test-individu
* Valider le formulaire
* Aller sur la page d'un logiciel (ex: Asqatasun, LibreOffice)
* Le bouton "Témoigner" ne doit pas appraître   

## 3. Poster un commentaire => Association

* Cliquer sur "Se connecter"
* Remplir les champs
    * Identifiant => [alias]-asso@comptoir-du-libre.org
    * Mot de passe => TEST-nonProfit
* Valider le formulaire
* Aller sur la page d'un logiciel (ex: Asqatasun, LibreOffice)
* Le bouton "Témoigner" ne doit pas appraître   
 