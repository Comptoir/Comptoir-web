# Ajouter un logiciel en tant que TEST-nonProfit -> Non profit user

## Ajouter un  logiciel en mode "Hors ligne"

* Se positionner sur la page "Tous les logiciels"
* Cliquer sur le bouton "Ajouter un logiciel"
    * Le clic doit renvoyer sur la page de connexion
* Se connecter sur le comptoir du libre
    * Identifiant => [alias]-asso@comptoir-du-libre.org
    * Mot de passe => TEST-nonProfit 
* Se positionner sur la page "Tous les logiciels"
* Cliquer sur le bouton "Ajouter un logiciel"
* Remplir les champs obligatoires (marqués d'une **"*"**)
* Valider le formulaire
* Vérifier la présence du message de validation
* Vérifier la présence du logiciel des la liste des derniers ajoutés [ici](http://comptoir-web.local/)

## Ajouter un logiciel en mode connecté

## Connexion simple

* Cliquer sur "Connexion"
* Se connecter sur le comptoir du libre
    * Identifiant => test-non-profit@adullact.org
    * Mot de passe => TEST-nonProfit 
    * Valider le formulaire
* Se positionner sur la page "Tous les logiciels"
* Cliquer sur le bouton "Ajouter un logiciel"
* Se positionner sur la page "Tous les logiciels"
* Cliquer sur le bouton "Ajouter un logiciel"
* Remplir les champs obligatoires (marqués d'une **"*"**)
* Valider le formulaire
* Vérifier la présence du message de validation
* Vérifier la présence du logiciel des la liste des derniers ajoutés [ici](http://comptoir-web.local/)
