# Ajouter un logiciel en tant qu'une Société

## Ajouter un  logiciel en mode "Hors ligne"

* Se positionner sur la page "Tous les logiciels"
* Cliquer sur le bouton "Ajouter un logiciel"
    * Le clic doit renvoyer sur la page de connexion
* Se connecter sur le comptoir du libre
    * Identifiant => [alias]-presta@comptoir-du-libre.org
    * Mot de passe => Test-MyCompany 
* Se positionner sur la page "Tous les logiciels"
* Cliquer sur le bouton "Ajouter un logiciel"
* Remplir les champs obligatoires (marqués d'une **"*"**)
* Valider le formulaire
* Vérifier la présence du message de validation
* Vérifier la présence du logiciel des la liste des derniers ajoutés [ici](http://comptoir-web.local/)


## Ajouter un logiciel en mode connecté

## Connexion simple

* Cliquer sur "Connexion"
* Se connecter sur le comptoir du libre
    * Identifiant => [alias]-presta@comptoir-du-libre.org
    * Mot de passe => Test-MyCompany 
    * Valider le formulaire
* Se positionner sur la page "Tous les logiciels"
* Cliquer sur le bouton "Ajouter un logiciel"
* Remplir les champs obligatoires (marqués d'une **"*"**)
* Valider le formulaire
* Vérifier la présence du message de validation
* Vérifier la présence du logiciel des la liste des derniers ajoutés [ici](http://comptoir-web.local/)
