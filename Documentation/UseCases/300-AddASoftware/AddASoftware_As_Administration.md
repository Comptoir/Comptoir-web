# Ajouter un logiciel en tant qu'une collectivité

## Ajouter un  logiciel en mode "Hors ligne"

## Ajouter un  logiciel en mode "Hors ligne"

* Se positionner sur la page "Tous les logiciels"
* Cliquer sur le bouton "Ajouter un logiciel"
    * Le clic doit renvoyer sur la page de connexion
* Se connecter sur le comptoir du libre
    * Identifiant => [alias]-collectivite@comptoir-du-libre.org
    * Mot de passe => TEST-Administration 
* Se positionner sur la page "Tous les logiciels"
* Cliquer sur le bouton "Ajouter un logiciel"
* Remplir les champs obligatoires (marqués d'une **"*"**)
* Valider le formulaire
* Vérifier la présence du message de validation
* Vérifier la présence du logiciel des la liste des derniers ajoutés [ici](http://comptoir-web.local/)

### Connexion

* Cliquer sur "Connexion"
* Se connecter sur le comptoir du libre
    * Identifiant => [alias]-collectivite@comptoir-du-libre.org
    * Mot de passe => TEST-Administration 
    * Valider le formulaire
    
### Ajouter un logiciel

* Se positionner sur la page "Tous les logiciels"
* Cliquer sur le bouton "Ajouter un logiciel"
* Remplir les champs obligatoires (marqués d'une **"*"**)
    * Nom : Test-Logiciel
    * Url du dépot : http://test-logiciel.git
    * Logo : 
    * Licence : 
* Valider le formulaire
* Vérifier la présence du message de validation
* Vérifier la présence du logiciel des la liste des derniers ajoutés [ici](http://comptoir-web.local/)