# Se connecter en tant que Collectivité

# Connexion

* Cliquer sur "Connexion"
* Remplir les champs
    * Identifiant => [alias]-presta@comptoir-du-libre.org
    * Mot de passe => Test-MyCompany
* Valider le formulaire

# Mot de passe oublié 

* Cliquer sur le lien "mot de pass perdu"
* Remplir le champ avec l'adresse email renseignée lors de l'enregistrement du l'utilisateur :
    * [alias]-presta@comptoir-du-libre.org
* Cliquer sur le lien présent dans le mail
* Renseigner les éléments du formaulaire 
    * Nouveau mot de passe : TEST-MyCompanyNew
    * Confirmer le mot de passe : TEST-MyCompanyNew
* Vérifier la présence du message de succès.