# Se connecter en tant que Collectivité

# Connexion

* Cliquer sur "Connexion"
* Remplir les champs
    * Identifiant => [alias]-collectivite@comptoir-du-libre.org
    * Mot de passe => TEST-Administration
* Valider le formulaire

# Mot de passe oublié 

* Cliquer sur le lien "mot de pass perdu"
* Remplir le champ avec l'adresse email renseignée lors de l'enregistrement du l'utilisateur :
    * [alias]-collectivite@comptoir-du-libre.org
* Cliquer sur le lien présent dans le mail
* Renseigner les éléments du formaulaire 
    * Nouveau mot de passe : TEST-AdministrationNew
    * Confirmer le mot de passe : TEST-AdministrationNew
* Vérifier la présence du message de succès.
