# Se connecter en tant que Collectivité

# Connexion

* Cliquer sur "Connexion"
* Remplir les champs
    * Identifiant => [alias]-individu@comptoir-du-libre.org
    * Mot de passe => Test-individu
* Valider le formulaire

# Mot de passe oublié 

* Cliquer sur le lien "mot de pass perdu"
* Remplir le champ avec l'adresse email renseignée lors de l'enregistrement du l'utilisateur :
    * [alias]-individu@comptoir-du-libre.org
* Cliquer sur le lien présent dans le mail
* Renseigner les éléments du formaulaire 
    * Nouveau mot de passe : TEST-individuNew
    * Confirmer le mot de passe : TEST-individuNew
* Vérifier la présence du message de succès.