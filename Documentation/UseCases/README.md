# Cas d'utilisations

## 0. Objectif

L'ensemble des documents liste des tests fonctionnels du Comptoir

## 1. Action Utilisateurs 

### 1.1 Un utilisateur **connecté** de type **Administration** peut effectuer les actions suivantes :

* Consulter l'ensemble du site
* Ajouter un logiciel
* Poster un commentaire sur un logiciel 
* Se d'éclarer comme utilisateur d'un logiciel
* S'enlever de la liste des utilisateurs pour un logiciel
* Se déclarer comme fournisseur de service sur un logiciel
* S'enlever de la liste des fournisseurs des services pour un logiciel


### 1.2 Un utilisateur **connecté** de type **Company** peut effectuer les actions suivantes :

* Consulter l'ensemble du site
* Ajouter un logiciel
* Se déclarer comme fournisseur de service sur un logiciel
* S'enlever de la liste des fournisseurs des services pour un logiciel

### 1.3 Un utilisateur **connecté** de type **Association** peut effectuer les actions suivantes :

* Consulter l'ensemble du site
* Ajouter un logiciel
* Se déclarer comme fournisseur de service sur un logiciel
* S'enlever de la liste des fournisseurs des services pour un logiciel

### 1.4 Un utilisateur **connecté** de type **Person** peut effectuer les actions suivantes :

* Consulter l'ensemble du site
* Ajouter un logiciel
* Se déclarer comme fournisseur de service sur un logiciel
* S'enlever de la liste des fournisseurs des services pour un logiciel



TYPE UTILISATEUR    ||  Consultation    || Ajout de logiciel    ||  Utilisateur de || Fournisseur de services ||

    PERSON          ||          V       ||          V           ||       X         ||           V             ||
    Company         ||          V       ||          V           ||       X         ||           V             ||
    NON PROFIT      ||          V       ||          V           ||       X         ||           V             ||
    Administration  ||          V       ||          V           ||       V         ||           V             ||