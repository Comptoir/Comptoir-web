# S'enregistrer sur le comptoir du libre

## En tant que personne

* Cliquer sur "Sign up"
* Remplir le formulaire
    * **Type d'utilisateur** => Person (i18n en cours)
    * **Nom** => TEST-individu
    * **Courriel** => [alias]-individu@comptoir-du-libre.org
    * Description => champ libre
    * **Mot de passe** => TEST-individu
    * **Confirmer le mot de passe** => TEST-individu
    * Avatar => mettre une image respectant les indications
* Valider le fomulaire
* Verifier l'apparition du message de bienvenue sur le Comptoir du libre.
* Vérifier que l'utilisateur soit bien redirigé sur la page d'accueil.

