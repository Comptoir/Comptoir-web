# S'enregistrer sur le comptoir du libre

## En tant que société

* Cliquer sur "Sign up"
* Remplir le formulaire
    * **Type d'utilisateur** => Company (i18n en cours)
    * **Nom** => TEST-MyCompany
    * **Courriel** => [alias]-presta@comptoir-du-libre.org
    * Description => champ libre
    * **Mot de passe** => TEST-MyCompany
    * **Confirmer le mot de passe** => TEST-MyCompany
    * Avatar => mettre une image respectant les indications
* Valider le fomulaire
* Verifier l'apparition du message de bienvenue sur le Comptoir de libre
* Vérifier que l'utilisateur soit bien redirigé sur la page d'accueil.