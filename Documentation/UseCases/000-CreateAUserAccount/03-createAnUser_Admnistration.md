# S'enregistrer sur le comptoir du libre

## En tant que collectivité 

* Cliquer sur "Sign up"
* Remplir le formulaire
    * **Type d'utilisateur** => Administration
    * **Nom** => TEST-Administration
    * **Courriel** => [alias]-collectivite@comptoir-du-libre.org
    * Description => champ libre
    * **Mot de passe** => "TEST-Administration"
    * **Confirmer le mot de passe** => "TEST-Administration"
    * Avatar => mettre une image respectant les indications
* Valider le fomulaire
* Verifier l'apparition du message de bienvenue sur le Comptoir de libre
* Vérifier que l'utilisateur soit bien redirigé sur la page d'accueil.
