# S'enregistrer sur le comptoir du libre

## En tant que personne

* Cliquer sur "Sign up"
* Remplir le formulaire
    * **Type d'utilisateur** => Association (i18n en cours)
    * **Nom** => TEST-nonProfit
    * **Courriel** => [alias]-asso@comptoir-du-libre.org
    * Description => champ libre
    * **Mot de passe** => TEST-nonProfit
    * **Confirmer le mot de passe** => TEST-nonProfit
    * Avatar => mettre une image respectant les indications
* Valider le fomulaire
* Verifier l'apparition du message de bienvenue sur le Comptoir du libre.
* Vérifier que l'utilisateur soit bien redirigé sur la page d'accueil.

