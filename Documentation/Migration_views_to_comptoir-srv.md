# Migration des vues dans Comtoir-srv

## Objectif

Evaluation de la **migration** de comptoir-web dans comptoir-srv.

## Migrations en POC

### Migration des Helpers

#### ListHelper

* Copié-collé
* Légères adaptations pour convenir aux retours des controlleurs de comptoir-srv (retour objet non json)
* Les adaptations ont engendrées le déplacement des fichiers images dans un autre dossier. (webroot/img)

#### ScoreHelper

* Aucune modification apportée pour le moment.

### Migration des templates Users

#### Index.ctp

Liste de tous les utilisateurs présents sur le comptoir du libre.

* Copié-collé du template
* Modification pour convenir au retour du controller

#### Autres templates.

* Copié-collé
* Aucune adaptation réalisée mais un premier affichage est déjà possible
* Voir la liste dans la partie liste des fichiers > Template > Users

### Migration des templates Softwares

#### Index.ctp

Liste de tous les softwares présent sur le comptoir du libre.

* Copié-collé du template
* Modification pour convenir au retour du controller

#### Autres templates.

* Copié-collé
* Aucune adaptation réalisée mais un premier affichage est déjà possible
* Voir la liste dans la partie liste des fichiers > Template > Softwares

## Tâches à faire pour la migrations

### Routage des urls

* Masquer api/v1 => rediriger les action de l'api vers une url simple lorsque la demande n'est pas en json/xml
* Changer comptoir-srv par comptoir
* gestion des langues (`en` / `fr`)

### Parcours utilisateur

* Refaire la partie connexion
* Revoir tous les liens (ex : "all softwares")

### Entité User

* Adapter les vues
  * index
  * view
* Adapter les formulaires à l'entité **User**
  * login
  * change_password
  * lost_password
  * add
  * edit
* Ne plus utiliser le Helper modifié mais celui de cakePhp
* Ajouter les messages d'aide au remplissage de formulaire

### Entité Software

* Adapter les vues
  * index
  * view
* Adapter les formulaires à l'entité **Softwares**
  * Add
  * Edit

* Ne plus utiliser le Helper modifié mais celui de cakePhp
* Ajouter les messages d'aide au remplissage de formulaire

### Interaction user / software

* Adapater les formulaires software :
    * Template
      * softwares
        * add-review -> devient le Reviews/add.ctp
        * screenshots
* Adapter le controller pour le retour HTML (voir la liste des templates)

### Controller User / Software / Screenshot / Review / ReslationshipsSoftwaresUsers

Faire évoluer le retour html. Toutes les actions :

* Users
  * index
  * login
  * view
  * add
  * Edit
  * servicesProvidersUsers
  * administrations
  * nonProfitUsers
  * usedSoftwares
  * backedSoftwares
  * createdSofwares
  * contributionsSoftwares
  * providerforSofwares
  * delete
  * register
  * logout
* Softwares
 * index
 * Edit
 * view
 * add
 * delete
 * usersSoftwares
 * reviewsSoftware
 * backersSoftwares
 * alternativeTo
 * contributorsSoftware
 * servicesProviders
 * workswellSoftwares
 * screenshots
 * request
 * getProjectById
 * lastAdded
 * getRelationshipsSoftwaresUsers
* Reviews
  * index
  * view
  * add
  * Edit
  * getReviewsBySoftwareId
  * delete
* Screenshots
  * index
  * view
  * add
  * Edit
  * getScreenshotsBySoftwareId
* ReslationshipsSoftwaresUsers
  * add
  * delete
  * getUsersOfRelationshipsBySoftwareId
  * getPorviersRelationshipsBySOftwareId
  * getRelationshipsBySOftwareId
  * getRelationshipsByUsersId


## Liste des Fichiers a migrer

* Locale => i18n
  * fr_FR : pot;po;mo;
    * dafault :
    * Forms
    * Home
    * Layout
    * SoftwareFormula
    * Softwares
    * Users
* Template :
    * Element
        * Navigation
          * navbarFixedTop
          * pagination
        * Softwares
          * metrics.ctp
    * Users
      * add
      * change_password
      * contributions_softwares
      * Edit
      * index
      * login
      * lost_password
      * non_profit_users
      * providerfor_softwares
      * used_softwares
      * users_list
      * view
    * Home
      * contact
      * index
      * participate
    * Softwares
      * add
      * add_review
      * alternative_to
      * formula
      * index
      * reviews_softwares
      * screenshots
      * search
      * services_providers
      * users_softwares
      * view
      * wokswell_software
    * Layout
        * Base.ctp
        * Home.ctp
    * Request
      * index
      * softwares_list
* Controllers :
    * Home
      * index
      * contact
      * participate
    * Request ? => surement pas utile avec l'évolution
    * Softwares
      * formula
* View
  * Helper
    * ListHelper
    * ScoreHelper
  * AppView.php
* Tests-U
    * ListHelper
    * ScoreHelper
* Css
    * summary_block_style.Css
    * Bootstrap
    * Font-awesome
* Favicon
* img > logos
* js
    * accordion.js
    * Bootstrap
