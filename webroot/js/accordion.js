/**
 * Created by jgauthier on 17/08/16.
 */
function expandCollapse() {
    var theHeaders = document.querySelectorAll('.expandCollapse h3'), i;

    for (i = 0; i < theHeaders.length; i++) {

        var thisEl = theHeaders[i],
            theId = 'panel-' + i;

        var thisTarget = thisEl.parentNode.querySelector('.panel');

        if (!thisTarget) {
            continue;
        }

        // Create the button
        thisEl.innerHTML = '<button aria-expanded="false" aria-controls="' + theId + '">' + thisEl.textContent + '</button>';

        // Create the expandable and collapsible list and make it focusable
        thisTarget.setAttribute('id', theId);
        thisTarget.setAttribute('aria-hidden', 'true');
    }

    // Make it click
    var theButtons = document.querySelectorAll('.expandCollapse button[aria-expanded][aria-controls]');

    for (i = 0; i < theButtons.length; i++) {

        theButtons[i].addEventListener('click', function (e) {
            var thisButton = e.target;
            var state = thisButton.getAttribute('aria-expanded') === 'false' ? true : false;

            thisButton.setAttribute('aria-expanded', state);

            document.getElementById(thisButton.getAttribute('aria-controls')).setAttribute('aria-hidden', !state);

        });
    }
}
expandCollapse();