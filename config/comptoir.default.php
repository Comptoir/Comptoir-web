<?php

return [

    'OpenGraph' => [
        'url' => COMPTOIR_SRV_HOST,
        'type' => 'website',
        'title' => 'Comptoir du libre',
        'description' => __d("Layout", "opengraph.description"),
        'image' => COMPTOIR_SRV_HOST . 'img/logos/logo_CDL_couleur_vecto_refactor.svg.png',
        'card' => 'summary_large_image'
    ],

    "Piwik" => <<<EOF

EOF

];
