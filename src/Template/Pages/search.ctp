<?php

$this->layout = 'base';

$this->assign('title', __d("Pages","{0} search.title {1}" ,[count($datas->softwares)+count($datas->users),$searchQuery]));

?>



<?php if (!empty( $datas ) ): ?>
    <?= "<h1>" . __d("Pages",
        "search.results",
        [count($datas->softwares)+count($datas->users)]).'<span class="text-muted"> "'.$searchQuery.'"</span>'. "</h1>"?>

    <section>
        <?php
        echo $this->Lists->block(
            \Cake\Utility\Hash::extract($datas->softwares, '{n}'),
            [
                "type" => "software",
                "linkParticipate" => [
                    "id" => null,
                    "action" => "add",
                    "text" => __d("Softwares", "Add a software in Comptoir du libre"),
                    "authorized" => true,
                ],
                "indicator" => [
                    "idTooltip" => "softwaresListId",
                    "indicatorMessage" => __d("Pages","search.softwaresList")
                ],
                "form" => $this->element("Pages/SearchFormSoftwares"),
                "title" => "<h2>". __d("Pages","search.softwares",count($datas->softwares)) .'<span class="text-muted"> "'.$searchQuery.'"</span>'. "</h2>",
                "titleAddMore" => __d("Softwares", "Add a software in Comptoir du libre"),
                "emptyMsg" => __d("Softwares", "No software matching with your request in Comptoir du libre, yet."),

            ],
            false
        ); ?>

    </section>

    <section>

        <?php

        echo $this->Lists->block(\Cake\Utility\Hash::extract($datas->users, '{n}'),
            [
                "type" => "user",
                "title" => "<h2>" . __d("Pages","search.users",count($datas->users)) .'<span class="text-muted"> "'.$searchQuery.'"</span>'."</h2>",
                "link" => [
                    "action" => "UsedSoftwares",
                    "participate" => __d("Users", "participate"),
                ],
                "linkParticipate" => [
                    "id" => null,
                    "action" => "add",
                    "text" => __d("Forms", "Sign up"),
                    "authorized" => true,
                ],
                "indicator" => [
                    "idTooltip" => "usersListId",
                    "indicatorMessage" => __d("Pages","search.usersList")
                ],
                "titleAddMore" => "" ,
                "emptyMsg" => __d("Users", "No user matching with your request in Comptoir du libre, yet."),

            ],
            false
        ); ?>
    </section>



<?php endif; ?>
