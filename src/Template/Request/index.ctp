<?php
$this->layout = 'base';

?>

<?= $this->start('requestResults') ?>
<?php if (isset($json_datas->results)) : ?>
   
    <div>
        
    </div>
    
<?php endif; ?>
<?= $this->end('requestResults') ?>


<!-- ITEM FOR CAROUSEL -->
<!-- ITEM FOR TOP -->
<?= $this->start('requestTopResults') ?>

    <?php for ($i=0 ;$i<$carousel_top ; $i++) : ?>
        <?php if ( $i == 0 )  : ?>
            <div class="item active">
        <?php endif;?>
        <?php if ( $i > 0 )  : ?>
            <div class="item">
        <?php endif;?>
            <?= $this->Html->image('logos/logo_adullact.JPG', ['class'=>"second-slide",'alt' => 'Second slide'])?>
            <div class="container">
                <div class="carousel-caption">
                    <?php if (isset($json_datas->results)) : ?>
                    <h1>
                        <?= $json_datas->results[$i]->name ?>
                    </h1>
                    <p>
                        <?= $json_datas->results[$i]->description ?>
                    </p>
                    <p>
                        <a class="btn btn-lg btn-primary" <?="href=".$json_datas->results[$i]->url?> role="button"><?= __("See it !") ?></a>
                    </p>
                    <?php endif; ?>

                </div>
            </div>
        </div>       

    <?php endfor; ?>
<?= $this->end('requestTopResults') ?>
