<?php
/**
 * Created by IntelliJ IDEA.
 * User: mpastor
 * Date: 26/07/16
 * Time: 15:00
 */

?>

<?php
$this->layout('base');
?>

<?php echo $this->Lists->block(
    \Cake\Utility\Hash::extract($user->contributionssoftwares, '{n}.software'),
    [
        "type" => "software",
        "title" => "<h1>" . __d("ElementEntity", "Contributor for") . "</h1>",
        "link" => [
            "id" => $user->id,
            "action" => "contributionsSoftwares",
            "participate" => __d("Layout", "Declare a software that {0} has contributed to.", $user->username),
        ],
    ],
    false
); ?>

