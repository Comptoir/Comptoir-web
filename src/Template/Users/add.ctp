<?php
$this->layout("base");

$this->assign('title', __d("Forms",'Create an account{0}', isset($message) ? " - ".__d("Forms",$message) : ""));
?>

<div class="row">
    <div class = "warning-form bg-warning col-xs-offset-3 col-xs-6">
            <?= __d("Forms","Fields marked with an asterisk ({0}) are required.",'<span class = "asterisk">*</span>'); ?>
    </div>
</div>

<div class="row">
    <div class = "col-xs-offset-3 col-xs-6">

        <?= $this->FormComptoir->create(null,['type' => 'file',"enctype"=>"multipart/form-data",'url' => ['controller' => 'Users', 'action' => 'add']]) ?>
        <fieldset>
            <legend><?= "<h1>" . __d("Forms",'Create an account') . "</h1>" ?></legend>
                <div  class = "form-group">
                    <label class = "control-label" for="user_type_id"><?=__d("Forms","{0} User type: ", '<span class = "asterisk">*</span>')?></label>
                    <div class="radio ">
                        <label>
                            <input type="radio" name="user_type_id" id="user_type_id-1" value="2" required="required">
                            <?= __d("Forms","Administration")?>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="user_type_id" id="user_type_id-2" value="4" required="required">
                            <?= __d("Forms","Person")?>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="user_type_id" id="user_type_id-3" value="5" required="required">
                            <?= __d("Forms","Company")?>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="user_type_id" id="user_type_id-4" value="6" required="required">
                            <?= __d("Forms","Association")?>
                        </label>
                    </div>
                </div>


            <div  <?= (isset( $errors->username->_isUnique )) ? 'class="form-group has-error "' : 'class="form-group"'?>>
            <label class = "control-label" for="username"><?=__d("Forms", "{0} Name: ",'<span class = "asterisk">*</span>')?></label>
                <?= $this->FormComptoir->input('username',
                    ["class"=>"form-control",
                        'label'=>false,
                        "required"=>"required",
                        "error"=> (isset( $errors->username->_isUnique )) ? $errors->username->_isUnique: null,
                        "escape" => false
                    ]);?>
            </div>
            <div  <?= (isset( $errors->url->_isUnique )) ? 'class="form-group has-error "' : 'class="form-group"'?>>
            <label class = "control-label" for="username"><?=__d("Forms", "Website URL: ")?></label>
                <?= $this->FormComptoir->input('url',
                    ["class"=>"form-control",
                        'label'=>false,
                        'type'=>'url',
                        "error"=> (isset( $errors->url->_isUnique )) ? $errors->url->_isUnique: null,
                        "escape" => false
                    ]);?>
            </div>
            <div  <?= (isset( $errors->email )) ? 'class="form-group has-error "' : 'class="form-group"'?>>
                <label class = "control-label" for="email"><?=__d("Forms","{0} Email: ", '<span class = "asterisk">*</span>')?></label>
                <?= $this->FormComptoir->input('email',
                    ["class"=>"form-control",
                    'label'=>false,
                    "required"=>"required",
                    "error"=> [
                        isset($errors->email->_isUnique) ? $errors->email->_isUnique : null,
                        isset($errors->email->email) ? $errors->email->email : null,
                    ]]) ?>
            </div>
            <div class='form-group'>
                <?= $this->FormComptoir->input('description',["type"=>"textarea","class"=>"form-control","label"=>__d("Forms","Description : ")]); ?>
            </div>
            <div class='form-group'>
                <?= $this->FormComptoir->input('role',['value' => 'User','type'=>'hidden',"required"=>"required"]); ?>
            </div>


            <div  <?= (isset( $errors->password->compare )) ? 'class="form-group has-error "' : 'class="form-group"'?>>

                <label  class = "control-label" for="password"><?= __d("Forms"," {0} Password : ", '<span class = "asterisk">*</span>')?></label>

                <?php echo  $this->FormComptoir->input(
                    'password',
                    [
                        "class"=>"form-control",
                        'label'=>false,
                        "required"=>"required",
                        "error"=> isset( $errors->password->compare ) ? $errors->password->compare : null,
                    ]
                ); ?>
            </div>
             <div  <?= (isset( $errors->password->compare )) ? 'class="form-group has-error "' : 'class="form-group"'?>>

                <label class = "control-label" for="password"><?= __d("Forms"," {0} Confirm password : ", '<span class = "asterisk">*</span>')?></label>
                <?= $this->FormComptoir->input('password',
                    ["class"=>"form-control",
                    'label'=>false,
                    "required"=>"required",
                    "name" => "confirm_password",
                    "id" => "confirm_password" ,
                    "error"=> isset( $errors->password->compare ) ? $errors->password->compare : null,
                    ]
                ); ?>
            </div>


            <div  <?= isset($errors->photo) && !is_string($errors->photo)? 'class="form-group has-error "' : 'class="form-group"'?>>
                <label class="control-label" for="photo"><?= __d("Forms", " {0} Avatar: ", '<span class = "asterisk">*</span>') ?></label>
                <?= $this->FormComptoir->input(
                    'photo',
                    [
                        'type' => 'file',
                        "label" => false,
                        "error"=>(isset($errors->photo) && !is_string($errors->photo)) ? (array)$errors->photo: null,

                    ]) ?>

            </div>
                <?php $help = '<div class="help-block">
                        <ul>';
                        !isset($user->photo->file) ?  $help .=  '<li>'.__d("Forms","Accepted formats JPEG, PNG, GIF, SVG.").'</li>' : "";
                        !isset($user->photo->fileBelowMaxWidth) || !isset($user->photo->fileBelowMaxHeigth) ?  $help .=  '<li>'.__d("Forms","Maximum size: 350x350px.").'</li>' : "";
                !isset($user->photo->fileBelowMaxSize) ? $help .= '<li>' . __d("Forms", "Maximum weight: 1{0}.", __d('Forms', "<abbr title='Megabit'>MB</abbr>")) . '</li>' : "";
                $help .='</ul>
                    </div>';
                echo $help;
                ?>
        </fieldset>
        <?= $this->FormComptoir->button(__d("Forms","Sign up"),["class"=>"btn btn-default addmore"]) ?>
        <?= $this->FormComptoir->end() ?>
    </div>

</div>