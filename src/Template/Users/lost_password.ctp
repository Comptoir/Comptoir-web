<?php
$this->layout("base");
$this->assign('title', __d("Forms",'Reset your password', isset($message) ? " - ".__d("Forms",$message) : ""));

?>


<div class="row">
    <div class = "col-xs-offset-3 col-xs-6">

        <?= $this->FormComptoir->create(null,['type' => 'file','url' => ['controller' => 'Users', 'action' => 'lostPassword']]) ?>
        <fieldset>
            <legend><?= __d("Forms",'Reset your password') ?></legend>
            <label class = "control-label" for="email"><?=__d("Forms","Users.lostPassword.email",'<span class = "asterisk">*</span>')?></label>

            <div <?= isset($message) && $message!="" ? 'class="form-group has-error "' : 'class="form-group"' ?>>
                <?php echo  $this->FormComptoir->input(
                    'email',
                    [
                        "class"=>"form-control",
                        'label'=>false,
                        "required"=>"required",
                        "error"=>isset($message) && $message!="" ? $message : null,
                    ]
                ); ?>
            </div>
        </fieldset>
        <?= $this->FormComptoir->button(__d("Forms","Send to me an email to reset my password."),["class"=>"btn btn-default addmore"]) ?>
        <?= $this->FormComptoir->end() ?>
    </div>
</div>