<?php
$this->layout('base');
$this->assign('title', __d("Users", "{0} is user of",$user->username));

?>

<?php echo $this->Lists->block(
    \Cake\Utility\Hash::extract($user->usedsoftwares, '{n}.software'),
    [
        "type" => "software",
        "title" => "<h1>" . __d("Users", "{0} is user of",$user->username) . "</h1>",
        "link" => [
            "id" => $user->id,
            "action" => "UsedSoftwares",
            "participate" => __d("Users", "Declare a software that is used by {0}.", $user->username),
        ],
        "titleAddMore" => __d("Users", "Declare a software that is used by {0}.", $user->username),
    ],
    false
); ?>