<?php
$this->layout("base");
$this->assign('title', __d("Forms",'Reset your password {0}', isset($message) ? " - ".$message : ""));
?>

<div class="row">
    <div class = "warning-form bg-warning col-xs-offset-3 col-xs-6">
        <?= __d("Forms","Fields marked with an asterisk ({0}) are required.",'<span class = "asterisk">*</span>'); ?>
    </div>
</div>

<div class="row">
    <div class = "col-xs-offset-3 col-xs-6">

        <?= $this->FormComptoir->create(null,['url' => ['controller' => 'Users', 'action' => 'lostPassword',$id]]) ?>
        <fieldset>
            <legend><?= __d("Forms",'Change your password') ?></legend>

            <div  <?= isset($user->password->compare ) ? 'class="form-group has-error "' : 'class="form-group"'?>>
                <label class = "control-label" for="new_password"><?=__d("Forms","{0} New password: ",'<span class = "asterisk">*</span>')?></label>
                <?php echo  $this->FormComptoir->input(
                    'password',
                    [
                        "class"=>"form-control",
                        'label'=>false,
                        "required"=>"required",
                        "name" => "new_password",

                        "error"=> isset( $user->password->compare ) ? (array)$user->password->compare : null,
                    ]
                ); ?>
            </div>
            <div  <?= isset($user->confirm_password->compare ) ? 'class="form-group has-error "' : 'class="form-group"'?>>
                <label class = "control-label" for="confirm_password"><?=__d("Forms","{0} Confirm new password: ",'<span class = "asterisk">*</span>')?></label>
                <?php echo  $this->FormComptoir->input(
                    'password',
                    [
                        "class"=>"form-control",
                        'label'=>false,
                        "required"=>"required",
                        "name" => "confirm_password",
                        "error"=> isset( $user->confirm_password->compare ) ? (array)$user->confirm_password->compare : null,
                    ]
                ); ?>
            </div>

        </fieldset>
        <?= $this->FormComptoir->button(__d("Forms","Save"),["class"=>"btn btn-default addmore"]) ?>
        <?= $this->FormComptoir->end() ?>
    </div>
</div>