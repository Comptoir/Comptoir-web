<?php
/**
 * Created by IntelliJ IDEA.
 * User: mpastor
 * Date: 26/07/16
 * Time: 15:00
 */

$this->layout('base');

$titleAndH1 = __d("Users", "Services provider for");

$this->assign('title', $titleAndH1);

echo $this->Lists->block(
    \Cake\Utility\Hash::extract($user->providerforsoftwares, '{n}.software'),
    [
        "type" => "software",
        "title" => "<h1>" . $titleAndH1 . "</h1>",
        "link" => [
            "id" => $user->id,
            "action" => "providerforSoftwares",
            "participate" => __d("Users", "Declare a software that {0} is a service provider for.", $user->username),
        ],
        "titleAddMore" => __d("Users", "Declare a software that {0} is a service provider for.", $user->username),
    ],
    false
);

?>