<?php

$this->layout = 'base';
$this->assign('title', __d("Users",'Users.index.title'));

?>

<?php if (!empty( $users ) ): ?>

        <section>

            <?php
            echo $this->Lists->block($users,
                [
                    "type" => "user",
                    "title" => "<h1>" . __d("Users","Users list for Comptoir du libre ({0})",count($users)) ."</h1>",
                    "link" => [
                        "action" => "UsedSoftwares",
                        "participate" => __d("Users", "participate"),
                    ],
                    "linkParticipate" => isset($linkParticipate["linkParticipate"]) ? $linkParticipate["linkParticipate"] : null,
                    "titleAddMore" => "" ,
                ],
                false
            ); ?>
        </section>
<?php endif; ?>
