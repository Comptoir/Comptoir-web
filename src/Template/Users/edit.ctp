<?php
$this->layout("base");
$this->assign('title', __d("Users","Edit your account : ", $user->username));
?>

<div class="row">
    <div class = "warning-form bg-warning col-xs-offset-3 col-xs-6">
        <?= __d("Forms","Fields marked with an asterisk ({0}) are required.",'<span class = "asterisk">*</span>'); ?>
    </div>
</div>

<div class="row">
    <div class = "col-xs-offset-3 col-xs-6">

        <?= $this->FormComptoir->create(null,['type' => 'file','url' => ['controller' => 'Users', 'action' => 'edit',isset($user->id)? $user->id :null]]) ?>
        <fieldset>
            <legend><?= "<h1>" .  __d("Forms",'Edit your account') . "</h1>"  ?></legend>
            <div <?= isset($errors->username->_isUnique) ? 'class="form-group has-error "' : 'class="form-group"' ?>>
                <label class = "control-label" for="username"><?=__d("Forms", "{0} Name: ",'<span class = "asterisk">*</span>')?></label>

                <?= $this->FormComptoir->input('username',
                    ["class"=>"form-control",
                    'label'=>false,
                    "required"=>"required",
                    "error"=> (isset( $errors->username->_isUnique )) ? $errors->username->_isUnique: null,
                    "value" => isset($user->username) ? $user->username : null,
                    ]
                );?>
            </div>
            <div <?= isset($errors->email->_isUnique) ? 'class="form-group has-error "' : 'class="form-group"' ?>>
                <label class = "control-label" for="email"><?=__d("Forms","{0} Email: ", '<span class = "asterisk">*</span>')?></label>

                <?= $this->FormComptoir->input('email',
                    ["class"=>"form-control",
                    'label'=>false,
                    "required"=>"required",
                    "error"=> (isset( $errors->email->_isUnique )) ? $errors->email->_isUnique : null,
                    "value" => isset($user->email) ? $user->email : null,
                    ]); ?>
            </div>
            <div class='form-group'>
                <?= $this->FormComptoir->input('description',
                    [
                        "type"=>"textarea",
                        "class"=>"form-control",
                        "label"=>__d("Forms","Description : "),
                        "value" => isset($user->description) ? $user->description : null,
                    ]); ?>
            </div>
            <div class='form-group'>
                <?= $this->FormComptoir->input('role',['value' => 'User','type'=>'hidden',"required"=>"required"]); ?>
            </div>

            <div class='form-group'>
                <?= $this->FormComptoir->input('userType',
                    [
                        'value' => isset($user->user_type_id) ? $user->user_type_id :  null,
                        'type'=>'hidden',
                        "required"=>"required"]); ?>
            </div>

            <div <?= isset($errors->photo->_isUnique) ? 'class="form-group has-error "' : 'class="form-group"' ?>>
                    <?= $this->FormComptoir->input('photo', [
                        "error"=> isset($errors->photo) ? (array)$errors->photo: null,
                        'type' => 'file',
                            "label"=>__d("Forms","Avatar : ")]); ?>
                    <?php $help = '<div class="help-block">
                        <ul>';
                    !isset($user->photo->file) ?  $help .=  '<li>'.__d("Forms","Accepted formats JPEG, PNG, GIF, SVG.").'</li>' : "";
                    !isset($user->photo->fileBelowMaxWidth) || !isset($user->photo->fileBelowMaxHeigth) ?  $help .=  '<li>'.__d("Forms","Maximum size: 350x350px.").'</li>' : "";
                    !isset($user->photo->fileBelowMaxSize) ? $help .= '<li>' . __d("Forms", "Maximum weight: 1{0}.", __d('Forms', "<abbr title='Megabit'>MB</abbr>")) . '</li>' : "";
                    $help .='</ul>
                    </div>';
                    echo $help;
                    ?>
            </div>

            <?php echo  $this->FormComptoir->input(
                'password',
                [
                    "type"=>"hidden",
                    "class"=>"form-control",
                    'label'=>__d("Forms",
                        "*Password : "),
                    "required"=>"required",
                    "value"=> ($this->request->session()->read('Auth.User.password')!= null) ? $this->request->session()->read('Auth.User.password') : null,
                ]
            ); ?>
        </fieldset>
        <?= $this->FormComptoir->button(__d("Forms","Save"),["class"=>"btn btn-default addmore"]) ?>
        <?= $this->FormComptoir->end() ?>

        <?=
            $this->Html->link(__d("Users","Change password"),
            ["controller"=> "Users", "action" => "changePassword",$user->id],
            ['escape' => false])
        ?>
    </div>
</div>