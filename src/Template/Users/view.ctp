<?php
$this->layout('base');
$this->assign('title', __d("Users", $user->username));

?>
<section class="row">
    <div class=" col-sm-6 col-md-4 col-lg-4 hidden-xs">
        <div class="size-logo-overview">
            <?php if (empty ($user->logo_directory) || empty($user->photo)) : ?>
                <?= $this->Html->link($this->Html->image("logos/User_placeholder.jpg",
                    ["alt" => $user->username, "class" => "img-responsive"]),
                    ['controller' => 'users', 'action' => 'view', $user->id],
                    ['escape' => false]) ?>
            <?php else : ?>
                <?= $this->Html->link($this->Html->image(COMPTOIR_SRV_URL . $user->logo_directory . DS . $user->photo,
                    ["alt" => $user->username, "class" => "img-responsive"]),
                    "#",// url of the entity web site.
                    ['escape' => false]) ?>
            <?php endif; ?>
        </div>

    </div>

    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
        <ul class="list-unstyled">
            <li>
                <h1>
                    <?= $user->username ?>
                </h1>
            </li>
            <li>
                <?php echo $user->url != "" ? $this->Html->link($user->url) : null ?>
            </li>
        </ul>
        <div>
            <p class="text-overflow">
                <?= $user->description ?>
            </p>
        </div>
    </div>
</section>
<!-- END of OVERVIEW-->
<?php /** TODO : Refactor contributor's section
 * <section>
 * <?php echo $this->Lists->softwareBlock(
 * \Cake\Utility\Hash::extract($user->contributionssoftwares, '{n}.software'),
 * [
 * "title" => __d("Users", "Contributor for"),
 * "link" => [
 * "id" => $user->id,
 * "action" => "contributionsSoftwares",
 * "participate" => __d("Users", " Add"),
 * ],
 * "tooManyMsg" => __d("Layout", "See all"),
 * "titleSeeAll" => __d("Users", "See all softwares which {0} has contributed to.", $user->username),
 * "titleAddMore" => __d("Users", "Declare a software that {0} has contributed to.", $user->username),
 * "emptyMsg" => __d("Users", "{0} do not contribute to any project, yet.", $user->username)
 * ]
 * ); ?>
 * </section>
 **/ ?>
<section>
    <?php
    $softwareBlock = "";

    $user->user_type->name != 'Company' ?
        $softwareBlock = $this->Lists->block(
            \Cake\Utility\Hash::extract($user->usedsoftwares, '{n}.software'),
            [
                "type" => "software",
                "title" => "<h2>" . __d("Users", "User of") . "</h2>",
                "link" => [
                    "id" => $user->id,
                    "action" => "UsedSoftwares",
                    "participate" => __d("Layout", " Add"),
                ],
                "indicator" => [
                    "idTooltip" => "softwareListUserOfId",
                    "indicatorMessage" => __d("Users", "users.softwareListUserOf", $user->username)
                ],
                "tooManyMsg" => __d("Layout", "See all"),
                "titleSeeAll" => __d("Users", "See all softwares used by {0}", $user->username),
                "titleAddMore" => __d("Users", "Declare a software that is used by {0}.", $user->username),
                "emptyMsg" => __d("Users", "{0} do not used a software, yet.", $user->username)
            ]
        )
        : "";
    echo $softwareBlock;
    ?>
</section>
<section>
    <?php
    $user->user_type->name != 'Company' ? $limit = true : $limit = false;

    echo $this->Lists->block(
        \Cake\Utility\Hash::extract($user->providerforsoftwares, '{n}.software'),
        [
            "type" => "software",
            "title" => "<h2>" . __d("Users", "Services provider for") . "</h2>",
            "link" => [
                "id" => $user->id,
                "action" => "providerforSoftwares",
                "participate" => __d("Layout", " Add"),
            ],
            "indicator" => [
                "idTooltip" => "ServiceProviderForSoftwareListId",
                "indicatorMessage" => __d("Users", "users.ServiceProviderForSoftwareList", $user->username)
            ],
            "tooManyMsg" => __d("Layout", "See all"),
            "titleSeeAll" => __d("Users", "See all softwares which {0} is a service provider for.", $user->username),
            "titleAddMore" => __d("Users", "Declare a software that {0} is a service provider for.", $user->username),
            "emptyMsg" => __d("Users", "{0} do not provide a software, yet.", $user->username)
        ],
        $limit
    ); ?>
</section>

