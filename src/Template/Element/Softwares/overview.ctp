<div class="col-sm-6 col-md-4 col-lg-4 hidden-xs">
    <div class="size-logo-overview">
        <?php if (empty ($software->logo_directory) || empty($software->photo)) : ?>
            <?= $this->Html->link($this->Html->image("logos/Softwarelogo_placeholder.jpg",
                ["alt" => $software->softwarename, "class" => "img-responsive"]),
                ['controller' => 'Softwares', 'action' => 'view', $software->id],
                ['escape' => false]) ?>
        <?php else : ?>
            <?= $this->Html->link($this->Html->image(COMPTOIR_SRV_URL . $software->logo_directory . DS . $software->photo,
                ["alt" => $software->softwarename, "class" => "img-responsive"]),
                ['controller' => 'Softwares', 'action' => 'view', $software->id],
                ['escape' => false]) ?>
        <?php endif; ?>
    </div>

</div>

<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
    <ul class="list-unstyled metric-score col-xs-12">
        
        <li>

            <div class="align">
                <h1>
                    <?= $software->softwarename ?>
                </h1>
                <div class="rating">
                    <?= $this->Rating->display($software->average_review,
                        [
                            "indicator"=>__d("Softwares","Software.rating.indicatorMessage",[count($software->reviews)])
                        ])?>
                </div>

            </div>

        </li>
        <li>
            <?php if ($software->url_website) : ?>
            <?= __d("Forms","Official web site:") . $this->Html->link($software->url_website,
                $software->url_website,
                ['escape' => false]) ?>

            <?php else :?>

            <?= __d("Forms","Official web site:") . __d("Forms","NA") ?>

            <?php endif; ?>

        </li>
        <li>
            <?= __d("Forms","Source code:") . $this->Html->link($software->url_repository,
                $software->url_repository,
                ['escape' => false]) ?>
        </li>
        <li>
            <p class="text-overflow">
                <?= $software->description ?>
            </p>
        </li>
        <li>
            <?= $this->Html->link(__d("Softwares","Edit"),
                ['controller' => $this->request->controller, 'action' => "edit",$software->id],
                ['class' => 'btn btn-default edit-software inline-block', ],
                ['escape' => false]) ?>
        </li>
    </ul>
</div>