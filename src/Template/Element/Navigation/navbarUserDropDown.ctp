<?php ?>

<?php if ($this->isHere("Users")
|| $this->isHere("Users", 'administrations')
|| $this->isHere("Users", 'nonProfitUsers')
): ?>
<li class="current">
    <?= $this->Html->link(__d("ElementNavigation", "All users") . '<span class ="caret"></span>',
        ['controller' => 'Users', 'action' => 'index',],
        [
            'escape' => false,
            "role" => "button",
        ]
    ) ?>
    <?php elseif (!$this->isHere("Users")): ?>
<li class="">
    <?= $this->Html->link(__d("ElementNavigation", "All users") . '<span class ="caret"> </span>',
        ['controller' => 'Users', 'action' => 'index',],
        [
            'escape' => false,
            "role" => "button",
        ]
    ) ?>
    <?php endif; ?>
    <ul class="">
        <?php if ($this->isHere("Users", "administrations")): ?>
    <li class="current">
    <span class=" text-center"><?= __d("ElementNavigation", "Administrations") ?></span>
    <?php elseif (!$this->isHere("Users", "administrations")): ?>
        <li class="">
            <?= $this->Html->link(__d("ElementNavigation", "Administrations"),
                ['controller' => 'Users', 'action' => 'administrations'],
                ['escape' => false]) ?>
            <?php endif; ?>
        </li>
        <?php if ($this->isHere("Users", "nonProfitUsers")): ?>
        <li class="current">
            <span class=" text-center"><?= __d("ElementNavigation", "Non profit users") ?></span>
            <?php elseif (!$this->isHere("Users", "non-profit-users")): ?>
        <li class="">
            <?= $this->Html->link(__d("ElementNavigation", "Non profit users"),
                ['controller' => 'Users', 'action' => 'nonProfitUsers'],
                ['escape' => false]) ?>
            <?php endif; ?>
        </li>
    </ul>
</li>