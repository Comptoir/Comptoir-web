<?php

/* 
* This is the pagination's code
 */
?>

<?php if ( $this->Paginator->param('pageCount') ) : ?>
<div class="paginator col-xs-12 col-sm-offset-2 col-mg-5 col-mg-offset-1 col-lg-9 col-lg-offset-4">
    <ul class="pagination pagination-lg visible-md visible-lg">
         <?= $this->Paginator->first('| ' . __d("ElementNavigation",'First')) ?>
         <?= $this->Paginator->prev('< ' . __d("ElementNavigation",'previous')) ?>

         <?= $this->Paginator->numbers(["modulus"=>4]) ?>

         <?= $this->Paginator->next(__d("ElementNavigation",'next') . ' >') ?>
         <?= $this->Paginator->last(__d("ElementNavigation",'Last') . ' |') ?>
    </ul>

    <ul class="pagination visible-sm">
         <?= $this->Paginator->first('| ' . __d("ElementNavigation",'First')) ?>
         <?= $this->Paginator->prev('< ' . __d("ElementNavigation",'previous')) ?>

         <?= $this->Paginator->numbers(["modulus"=>4]) ?>


         <?= $this->Paginator->next(__d("ElementNavigation",'next') . ' >') ?>
         <?= $this->Paginator->last(__d("ElementNavigation",'Last') . ' |') ?>
    </ul>

    <ul class="pagination pagination-sm visible-xs">
         <?= $this->Paginator->first('| ' . __d("ElementNavigation",'First')) ?>
         <?= $this->Paginator->prev(__d("ElementNavigation",'<<')) ?>

         <?= $this->Paginator->numbers(["modulus"=>4]) ?>

         <?= $this->Paginator->next(__d("ElementNavigation",'>>')) ?>
         <?= $this->Paginator->last(__d("ElementNavigation",'Last') . ' |') ?>
    </ul>
</div>
<p><?= $this->Paginator->counter() ?></p> 
<?php endif; ?>