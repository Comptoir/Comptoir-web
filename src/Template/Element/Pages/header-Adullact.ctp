
<div class="container-fluid" id="parentship">
    <div class="row">
        <div class="col-sm-12">
            <span>
                <?= __d("Pages","Paternity.message")?>
            </span>
            <?= $this->Html->link(
                $this->Html->image("logos/Logo_adullact_trunked.png",
                    ["alt" => "Adullact","id" => "parentship-logo"]),
                "http://www.adullact.org",
                ['escape' => false ,"target"=>"_blank"]) ?>
        </div>
    </div>
</div>

