<?php

/**
 * This template conatins all informations for the header
 */
?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- ****** faviconit.com favicons ****** -->
    <?= $this->Html->meta('favicon.ico', 'favicon/CDL-Favicon.ico', ['type' => 'icon', 'rel' => 'shortcut icon']); ?>
    <?= $this->Html->meta('favicon.ico', 'favicon/CDL-Favicon.16_16.ico', ['type' => 'icon', "sizes" => "16x16 32x32 64x64"]); ?>
    <?= $this->Html->meta('favicon.ico', 'favicon/CDL-CDL-Favicon.196_196.png', ['type' => 'icon', "sizes" => "196x196"]); ?>
    <?= $this->Html->meta('favicon.ico', 'favicon/CDL-Favicon.160_160.png', ['type' => 'icon', "sizes" => "160x160"]); ?>
    <?= $this->Html->meta('favicon.ico', 'favicon/CDL-Favicon.96_96.png', ['type' => 'icon', "sizes" => "96x96"]); ?>
    <?= $this->Html->meta('favicon.ico', 'favicon/CDL-Favicon.64_64.png', ['type' => 'icon', "sizes" => "64x64"]); ?>
    <?= $this->Html->meta('favicon.ico', 'favicon/CDL-Favicon.32_32.png', ['type' => 'icon', "sizes" => "32x32"]); ?>
    <?= $this->Html->meta('favicon.ico', 'favicon/CDL-Favicon.16_16.png', ['type' => 'icon', "sizes" => "16x16"]); ?>
    <?= $this->Html->meta('favicon.ico', 'favicon/CDL-Favicon.svg', ['type' => 'icon', 'rel' => 'apple-touch-icon']); ?>
    <?= $this->Html->meta('favicon.ico', 'favicon/CDL-Favicon.114_114.png', ['type' => 'icon', 'rel' => 'apple-touch-icon', 'sizes' => '114x114']); ?>
    <?= $this->Html->meta('favicon.ico', 'favicon/CDL-Favicon.72_72.png', ['type' => 'icon', 'rel' => 'apple-touch-icon', 'sizes' => '72x72']); ?>
    <?= $this->Html->meta('favicon.ico', 'favicon/CDL-Favicon.144_144.png', ['type' => 'icon', 'rel' => 'apple-touch-icon', 'sizes' => '144x144']); ?>
    <?= $this->Html->meta('favicon.ico', 'favicon/CDL-Favicon.60_60.png', ['type' => 'icon', 'rel' => 'apple-touch-icon', 'sizes' => '60x60']); ?>
    <?= $this->Html->meta('favicon.ico', 'favicon/CDL-Favicon.120_120.png', ['type' => 'icon', 'rel' => 'apple-touch-icon', 'sizes' => '120x120']); ?>
    <?= $this->Html->meta('favicon.ico', 'favicon/CDL-Favicon.76_76.png', ['type' => 'icon', 'rel' => 'apple-touch-icon', 'sizes' => '76x76']); ?>
    <?= $this->Html->meta('favicon.ico', 'favicon/CDL-Favicon.152_152.png', ['type' => 'icon', 'rel' => 'apple-touch-icon', 'sizes' => '152x152']); ?>
    <?= $this->Html->meta('favicon.ico', 'favicon/CDL-Favicon.180_180.png', ['type' => 'icon', 'rel' => 'apple-touch-icon', 'sizes' => '180x180']); ?>

    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="favicon/favicon-144.png">
    <meta name="msapplication-config" content="favicon/browserconfig.xml">

    <?= $this->fetch('title') ?  '<title>' . $this->fetch('title') . '</title>' : '<title>' . __d("Layout","Comptoir du libre")  . '</title>'  ?>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php // OPEN GRAPH ?>
    <?php if (isset($openGraph)): ?>
        <?= $this->OpenGraph
        ->setTitle($openGraph["title"])
        ->setDescription($openGraph["description"])
        ->setImage($openGraph["image"], $openGraph["imageSize"])
        ->render(); ?>
    <?php endif ; ?>

    <?php if (isset($card)): ?>
    <?= $this->Card
        ->setTitle($card["title"])
        ->setCard(\Cake\Core\Configure::read("OpenGraph.card"))
        ->setDescription($card["description"])
        ->setImage($card["image"], $card["imageSize"])
        ->render(); ?>
    <?php endif ; ?>

    <?= $this->Html->css('bootstrap/css/bootstrap.min.css') ?>

    <?= $this->Html->css('font-awesome-4.6.3/css/font-awesome.min.css') ?>

    <?php // $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js') ?>
    <?= $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js') ?>

    <?= $this->Html->script('bootstrap/js/bootstrap.min.js') ?>
    <?= $this->Html->script('bootstrap/js/simpleAccessible.js') ?>


    <?= $this->Html->css('dashboard.css') ?>

    <?= $this->Html->css('comptoir.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>