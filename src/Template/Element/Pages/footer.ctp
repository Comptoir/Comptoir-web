<?php

/**
 * This template conatins all informations for the footer
 */
?>

<footer class="row">
    <?php echo __d("Pages", "Comptoir du Libre version ") . \Cake\Core\Configure::read("VERSION.footer") ?> -

    <?php if ($this->request->isHere("legal")): ?>
        <span><?= __d("Home", "Legal") ?></span>
    <?php elseif (!$this->request->isHere("legal")): ?>
        <?= $this->Html->link(__d("Home", "Legal"),
            ['controller' => 'Home', 'action' => 'legal'],
            ['escape' => false]) ?> -
    <?php endif; ?>

    <?php if ($this->request->isHere("accessibility")): ?>
        <span><?= __d("Home", "Accessibility") ?></span>
    <?php elseif (!$this->request->isHere("contact")): ?>
        <?= $this->Html->link(__d("Home", "Accessibility"),
            ['controller' => 'Home', 'action' => 'accessibility'],
            ['escape' => false]) ?> -
    <?php endif; ?>

    <?php if ($this->request->isHere("contact")): ?>
        <span><?= __d("ElementNavigation", "Contact") ?></span>
    <?php elseif (!$this->request->isHere("contact")): ?>
        <?= $this->Html->link(__d("ElementNavigation", "Contact"),
            ['controller' => 'Home', 'action' => 'contact'],
            ['escape' => false]) ?>
    <?php endif; ?> -

    <?php foreach ($availableLanguages as $lang => $alias): ?>
        <?php if ($alias === $selectedLanguage) continue; ?>
        <?php $pass = $this->request->param('pass') ?>
        <span>
            <?= $this->Html->link(ucfirst($explicitLanguages[$lang]), [
                'language' => $lang,
                'controller' => $this->request->param('controller'),
                'action' => $this->request->param('action'),
                '?' => $this->request->query,
                isset($pass[0]) ? $pass[0] : null
            ], ["lang" => $lang, "title" => __d("Pages", "menu.switch")]) ?>
        </span>
    <?php endforeach; ?>
</footer>

