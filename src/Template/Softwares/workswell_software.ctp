<?php
/**
 * Created by IntelliJ IDEA.
 * User: mpastor
 * Date: 26/07/16
 * Time: 15:00
 */

?>

<?php
$this->layout('base');

$this->assign('title', __d("Softwares", "Working well with {0}", $software->softwarename));

?>



<section>
    <?php
    echo $this->Lists->block(
        \Cake\Utility\Hash::extract($software->workswellsoftwares, '{n}.software'),
        [
            "type" => "software",
            "title" => "<h1>" . __d("Softwares", "Working well with {0}", $software->softwarename) . "</h1>",
            "link" => [
                "id" => $software->id,
                "action" => "workswellSoftware",
                "participate" => __d("Softwares", "Declare a software that works well with {0}.", $software->softwarename),
            ],
            "titleAddMore" => __d("Softwares", "Add a software that works well with {0}.", $software->softwarename),
        ],
        false
    ); ?>
</section>