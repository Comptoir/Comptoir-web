<?php
$this->layout("base");
$this->assign('title', __d("Forms",'Edit {0}',$software->softwarename, isset($message) ? " - ".$message : "") );
?>

<div class="row">
    <div class = "warning-form bg-warning col-xs-offset-3 col-xs-6">
        <?= __d("Forms","Fields marked with an asterisk ({0}) are required.",'<span class = "asterisk">*</span>'); ?>
    </div>
</div>

<div class="row">
    <div class="reviews form col-xs-offset-3 col-xs-6 columns content">
        <div class="softwares form large-9 medium-8 columns content">
            <?= $this->FormComptoir->create(null, ['type' => 'file',"enctype" => "multipart/form-data", 'url' => ['controller' => 'Softwares', 'action' => 'edit',isset($software->id)? $software->id :null]]) ?>
            <fieldset>
                <legend><?= "<h1>" .  __d("Forms",'Edit {0}' ,$software->softwarename)."</h1>"  ?></legend>

                <div <?= isset($errors->softwarename->_isUnique) ? 'class="form-group has-error "' : 'class="form-group"' ?>>
                    <label class="control-label" for="softwarename"><?= __d("Forms", " {0} Software name: ", '<span class = "asterisk">*</span>') ?></label>
                    <?= $this->FormComptoir->input(
                        'softwarename',
                        [
                            "class" => "form-control",
                            "label" => false,
                            "required"=>"required",
                            "value" => isset($software->softwarename) ? $software->softwarename : null,
                            "error"=>(isset($errors->softwarename->_isUnique)) ? (array)$errors->softwarename->_isUnique: null,
                        ]); ?>
                </div>
                <div <?= (isset($errors->url_website->_isUnique) || isset($errors->url_website->url)) ? 'class="form-group has-error "' : 'class="form-group"' ?>>
                    <label class="control-label"for="url_website"><?= __d("Forms", " {0} URL of official web site", '<span class = "asterisk">*</span>') ?></label>
                    <?= $this->FormComptoir->input(
                        'url_website',
                        [
                            "type"=>"url",
                            "class" => "form-control",
                            "label" => false,
                            "required"=>"required",
                            "value" => isset($software->url_website) ? $software->url_website : null,
                            //TODO: c'est moche il faudra changer ça
                            "error"=>(isset($errors->url_website->_isUnique)) ? (array)$errors->url_website->_isUnique: null,
                            "error"=>(isset($errors->url_website->url)) ? $errors->url_website->url : null,
                        ]) ?>
                </div>
                <div <?= isset($errors->url_repository->_isUnique) || isset($errors->url_repository->url) ? 'class="form-group has-error "' : 'class="form-group"' ?>>
                    <label class="control-label"for="url_repository"><?= __d("Forms", " {0} URL of source code repository", '<span class = "asterisk">*</span>') ?></label>
                    <?= $this->FormComptoir->input(
                        'url_repository',
                        [
                            "type"=>"url",
                            "class" => "form-control",
                            "label" => false,
                            "required"=>"required",
                            "value" => isset($software->url_repository) ? $software->url_repository : null,
                            //TODO: c'est moche il faudra changer ça
                            "error"=>(isset($errors->url_repository->_isUnique)) ? (array)$errors->url_repository->_isUnique: null,
                            "error"=>(isset($errors->url_repository->url)) ? (array)$errors->url_repository->url: null,
                        ]) ?>
                </div>
                <div <?= isset($errors->description->error) ? 'class="form-group has-error "' : 'class="form-group"' ?>>
                    <label class="control-label" for="description"><?= __d("Forms", "Description: ") ?></label>
                    <?= $this->FormComptoir->input('description', [
                            "type"=>"textarea",
                            "class" => "form-control",
                            "value" => isset($software->description) ? $software->description : null,
                            "label" => false])?>
                </div>
                <div  <?= isset($errors->photo) ? 'class="form-group has-error "' : 'class="form-group"'?>>
                    <label class="control-label" for="photo"><?= __d("Forms", " {0} Logo: ", '<span class = "asterisk">*</span>') ?></label>
                    <?= $this->FormComptoir->input(
                        'photo',
                        [
                            'type' => 'file',
                            "label" => false,
                            "error"=> isset($errors->photo) ? (array)$errors->photo: null,

                        ]) ?>

                </div>
                    <?php $help = '<div class="help-block">
                            <ul>';
                    !isset($software->photo->file) ?  $help .=  '<li>'.__d("Forms","Accepted formats JPEG, PNG, GIF, SVG.").'</li>' : "";
                    !isset($software->photo->fileBelowMaxWidth) || !isset($software->photo->fileBelowMaxHeigth) ?  $help .=  '<li>'.__d("Forms","Maximum size: 350x350px.").'</li>' : "";
                    !isset($software->photo->fileBelowMaxSize) ? $help .= '<li>' . __d("Forms", "Maximum weight: 1{0}.", __d('Forms', "<abbr title='Megabit'>MB</abbr>")) . '</li>' : "";
                    $help .='</ul>
                        </div>';
                    echo $help;
                    ?>

                <div <?= isset($errors->licence) ? 'class="form-group has-error "' : 'class="form-group"' ?>>
                    <label class="control-label" for="licence_id"><?= __d("Forms", " {0} Licenses: ", '<span class = "asterisk">*</span>') ?></label>
                    <?= $this->FormComptoir->input(
                        'licence_id',
                        ['options' => $licenses,
                            "value" => isset($software->licence_id) ? $software->licence_id: null,

                            'empty' => false,
                            "class" => "form-control",
                            "label" => false,
                            "errors" => isset($errors->licence) ? $software->licence : null,
                        ]) ?>
                </div>



            </fieldset>
            <?= $this->Form->button(__d("Forms",'Submit'),["class"=>"btn btn-default addmore"]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>