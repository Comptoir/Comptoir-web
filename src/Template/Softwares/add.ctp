<?php
$this->layout("base");
$this->assign('title', __d("Forms",'Add a software{0}', isset($message) ? " - ".$message : ""));
?>

<div class="row">
    <div class = "warning-form bg-warning col-xs-offset-3 col-xs-6">
        <?= __d("Forms","Fields marked with an asterisk ({0}) are required.",'<span class = "asterisk">*</span>'); ?>
    </div>
</div>

<div class="row">
    <div class="reviews form col-xs-offset-3 col-xs-6 columns content">
        <div class="softwares form large-9 medium-8 columns content">
            <?= $this->FormComptoir->create(null, ['type' => 'file',"enctype" => "multipart/form-data", 'url' => ['controller' => 'Softwares', 'action' => 'add']]) ?>
            <fieldset>
                <legend><?= "<h1>" . __d("Softwares", 'Add Software')."</h1>"  ?></legend>

                <div <?= isset($software->softwarename->_isUnique) ? 'class="form-group has-error "' : 'class="form-group"' ?>>
                    <label class="control-label" for="softwarename"><?= __d("Forms", " {0} Software name: ", '<span class = "asterisk">*</span>') ?></label>
                    <?= $this->FormComptoir->input(
                        'softwarename',
                        [
                            "class" => "form-control",
                            "label" => false,
                            "required"=>"required",
                            "error"=>(isset($software->softwarename->_isUnique)) ? (array)$software->softwarename->_isUnique: null,
                        ]); ?>
                </div>
                <div <?= isset($software->url_website->_isUnique) ? 'class="form-group has-error "' : 'class="form-group"' ?>>
                    <label class="control-label"for="url_website"><?= __d("Forms", " {0} URL of official web site", '<span class = "asterisk">*</span>') ?></label>
                    <?= $this->FormComptoir->input(
                        'url_website',
                        [
                            "type"=>"url",
                            "class" => "form-control",
                            "label" => false,
                            "required"=>"required",
                            "error"=>(isset($software->url_website->_isUnique)) ? (array)$software->url_website->_isUnique: null,
                        ]) ?>
                </div>
                <div <?= isset($software->url_repository->_isUnique) ? 'class="form-group has-error "' : 'class="form-group"' ?>>
                    <label class="control-label"for="url_repository"><?= __d("Forms", " {0} URL of source code repository", '<span class = "asterisk">*</span>') ?></label>
                    <?= $this->FormComptoir->input(
                        'url_repository',
                        [
                            "type"=>"url",
                            "class" => "form-control",
                            "label" => false,
                            "required"=>"required",
                            "error"=>(isset($software->url_repository->_isUnique)) ? (array)$software->url_repository->_isUnique: null,
                        ]) ?>
                </div>
                <div <?= isset($software->description->error) ? 'class="form-group has-error "' : 'class="form-group"' ?>>
                    <label class="control-label" for="description"><?= __d("Forms", "Description: ") ?></label>
                    <?= $this->FormComptoir->input('description', ["type"=>"textarea","class" => "form-control", "label" => false])?>
                </div>
                <div  <?= isset($software->photo) && !is_string($software->photo)? 'class="form-group has-error "' : 'class="form-group"'?>>
                    <label class="control-label" for="photo"><?= __d("Forms", " {0} Logo: ", '<span class = "asterisk">*</span>') ?></label>
                    <?= $this->FormComptoir->input(
                        'photo',
                        [
                            'type' => 'file',
                            "label" => false,
                            "error"=>(isset($software->photo) && !is_string($software->photo)) ? (array)$software->photo: null,

                        ]) ?>

                </div>
                <?php $help = '<div class="help-block">
                        <ul>';
                !isset($software->photo->file) ?  $help .=  '<li>'.__d("Forms","Accepted formats JPEG, PNG, GIF, SVG.").'</li>' : "";
                !isset($software->photo->fileBelowMaxWidth) || !isset($software->photo->fileBelowMaxHeigth) ?  $help .=  '<li>'.__d("Forms","Maximum size: 350x350px.").'</li>' : "";
                !isset($software->photo->fileBelowMaxSize) ? $help .= '<li>' . __d("Forms", "Maximum weight: 1{0}.", __d('Forms', "<abbr title='Megabit'>MB</abbr>")) . '</li>' : "";
                $help .='</ul>
                    </div>';
                echo $help;
                ?>

                <div <?= isset($software->licence) ? 'class="form-group has-error "' : 'class="form-group"' ?>>
                    <label class="control-label" for="licence_id"><?= __d("Forms", " {0} Licenses: ", '<span class = "asterisk">*</span>') ?></label>
                    <?= $this->FormComptoir->input(
                        'licence_id',
                        ['options' => $licenses,
                            'empty' => false,
                            "class" => "form-control",
                            "label" => false,
                        ]) ?>
                </div>
            </fieldset>
            <?= $this->Form->button(__d("Forms",'Submit'),["class"=>"btn btn-default addmore"]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>