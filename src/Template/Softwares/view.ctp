<?php
$this->layout('base');

$this->assign('title', __d("Softwares","{0}", $software->softwarename));

?>
<!-- ------------------------------------------ Overview ----------------------------------------------------------- -->
                <section class="row clearfix">
                    <?php echo $this->element("Softwares/overview") ?>
                </section>
<!-- ------------------------------------------------ End overview ------------------------------------------------ -->

                <section class="clearfix">
                    <?php echo $this->Lists->block(

                        \Cake\Utility\Hash::extract($software->screenshots, '{n}'),
                        [
                            "type" => "screenshot",
                            "title" =>"<h2>" . __d("Softwares", "Screenshots of {0}", $software->softwarename) . "</h2>",
                            "link" => [
                                "id" => $software->id,
                                "action" => "screenshots",
                                "participate" => __d("Layout"," Add"),
                            ],
//                            "linkParticipate" => [
//                                "id" => $software->id,
//                                "action" => "",
//                                "text" => __d("Softwares", "Add a screenshot for {0}.", $software->softwarename),
//                            ],
                            "indicator" => [
                                "idTooltip" => "sreenshotsListId",
                                "indicatorMessage" => __d("Softwares","softwares.sreenshotsList",$software->softwarename)
                            ],
                            "tooManyMsg" => __d("Layout", "See all ({0} screenshots)",count(\Cake\Utility\Hash::extract($software->screenshots, '{n}'))),
                            'titleSeeAll' => __d("Softwares", "See all screenshots of {0}", $software->softwarename),
                            "titleAddMore" => __d("Softwares", "Softwares.Screenshots.addMessage", $software->softwarename),
                            "emptyMsg" => __d("Softwares", "No screenshot for {0}.", $software->softwarename)
                        ]
                    ); ?>
                </section>

                <section class="clearfix">
                    <?php
                    echo $this->Lists->block(
                        \Cake\Utility\Hash::extract($software->reviews, '{n}'),
                        [
                            "type" => "review",
                            "title" => "<h2>" . __d("Softwares", "Reviews for {0}", $software->softwarename) . "</h2>",
                            "link" => [
                                "id" => $software->id,
                                "action" => "reviewsSoftware",
                                "participate" => __d("Layout", " Add"),
                            ],
                            "linkParticipate" => [
                                "id" => $software->id,
                                "action" => "addReview",
                                "text" => __d("Softwares", "Sofwares.Review.addMessage"),
                            ],
                            "indicator" => [
                                "idTooltip" => "reviewsListId",
                                "indicatorMessage" => __d("Softwares","softwares.reviewsList",$software->softwarename)
                            ],
                            "tooManyMsg" => __d("Layout", "See all ({0} reviews)",count(\Cake\Utility\Hash::extract($software->reviews, '{n}'))),
                            "titleSeeAll" => __d("Softwares", "See all reviews of {0}", $software->softwarename),
                            "titleAddMore" => __d("Softwares", "Sofwares.Review.addMessage", $software->softwarename),
                            "emptyMsg" => __d("Softwares", "No review for {0}.", $software->softwarename)
                        ]
                    ); ?>
                </section>

                <section>
                    <?php echo $this->Lists->block(
                        \Cake\Utility\Hash::extract($software->providerssoftwares, '{n}.user'),
                        [
                            "type" => "user",
                            "title" => "<h2>" . __d("Softwares", "Service providers for {0}", $software->softwarename) . "</h2>",
                            "link" => [
                                "id" => $software->id,
                                "action" => "servicesProviders",
                                "participate" => __d("Layout", " Add"),
                            ],
                            "linkParticipate" => [
                                "id" => $software->id,
                                "action" => "addServicesProvider",
                                "text" => __d("Softwares", "Softwares.Users.DelcareAs.serviceProvider.addMessage", $software->softwarename),
                            ],
                            "linkFallBack" => [
                                "id" => $software->id,
                                "action" => "fallBackServicesProvider",
                                "text" => __d("Softwares", "Softwares.Users.DelcareAs.serviceProvider.removeMessage", $software->softwarename),
                            ],
                            "indicator" => [
                                "idTooltip" => "serviceProvidersOfListId",
                                "indicatorMessage" => __d("Softwares","softwares.serviceProvidersOfList",$software->softwarename)
                            ],
                            "tooManyMsg" => __d("Layout", "See all ({0} services providers)",count(\Cake\Utility\Hash::extract($software->providerssoftwares, '{n}.user'))),
                            "titleSeeAll" => __d("Softwares", "See all service providers of {0}", $software->softwarename),
                            "titleAddMore" => __d("Softwares", "Softwares.Users.DelcareAs.serviceProvider.addMessage", $software->softwarename),
                            "titleFallBack" => __d("Softwares", "Softwares.Users.DelcareAs.serviceProvider.removeMessage", $software->softwarename),
                            "emptyMsg" => __d("Softwares", "No service provider for {0}.", $software->softwarename)
                        ]
                    ); ?>
                </section>

                <section>
                    <?php echo $this->Lists->block(
                        \Cake\Utility\Hash::extract($software->userssoftwares, '{n}.user'),
                        [
                            "type" => "user",
                            "title" => "<h2>" . __d("Softwares", "Users of {0}", $software->softwarename) . "</h2>" ,
                            "link" => [
                                "id" => $software->id,
                                "action" => "usersSoftware",
                                "participate" => __d("Layout", " Add"),
                            ],
                            "linkParticipate" => [
                                "id" => $software->id,
                                "action" => "addUserOf",
                                "text" => __d("Softwares", "Softwares.Users.DelcareAs.user.addMessage", $software->softwarename),
                            ],
                            "linkFallBack" => [
                                "id" => $software->id,
                                "action" => "fallBackusersSoftware",
                                "text" => __d("Softwares", "Softwares.Users.DelcareAs.user.removeMessage", $software->softwarename),
                            ],
                            "indicator" => [
                                "idTooltip" => "usersOfListId",
                                "indicatorMessage" => __d("Softwares","softwares.usersOfList",$software->softwarename)
                            ],
                            "tooManyMsg" => __d("Layout", "See all ({0} users)",count(\Cake\Utility\Hash::extract($software->userssoftwares, '{n}.user'))),
                            "titleSeeAll" => __d("Softwares", "See all declared users of {0}", $software->softwarename),
                            "titleAddMore" => __d("Softwares", "Softwares.Users.DelcareAs.user.addMessage", $software->softwarename),
                            "titleFallBack" => __d("Softwares", "Softwares.Users.DelcareAs.user.removeMessage.", $software->softwarename),
                            "emptyMsg" => __d("Softwares", "No user for {0}", $software->softwarename)
                        ]
                    ); ?>
                </section>

                <section>
                    <?php echo $this->Lists->block(
                        \Cake\Utility\Hash::extract($software->alternativeto, '{n}.software'),
                        [
                            "type" => "software",
                            "title" => "<h2>" . __d("Softwares", "Alternative to {0}", $software->softwarename) . "</h2>" ,
                            "link" => [
                                "id" => $software->id,
                                "action" => "alternativeTo",
                                "participate" => __d("Layout", " Add"),
                            ],
//                            "linkParticipate" => [
//                                "id" => $software->id,
//                                "action" => "",
//                                "text" => __d("Softwares", "Add a software that is an alternative to {0}.", $software->softwarename),
//                            ],
                            "indicator" => [
                                "idTooltip" => "softwaresAlternativeToListId",
                                "indicatorMessage" => __d("Softwares","softwares.softwaresAlternativeToList",$software->softwarename)
                            ],
                            "tooManyMsg" => __d("Layout", "See all ({0} softwares)",count(\Cake\Utility\Hash::extract($software->alternativeto, '{n}.software'))),
                            "titleSeeAll" => __d("Softwares", "See all alternatives to {0}", $software->softwarename),
                            "titleAddMore" => __d("Softwares", "Add a software that is an alternative to {0}.", $software->softwarename),
                            "emptyMsg" => __d("Softwares", "No alternative to {0}.", $software->softwarename)
                        ]
                    ); ?>
                </section>

                <section>
                    <?php echo $this->Lists->block(
                        \Cake\Utility\Hash::extract($software->workswellsoftwares, '{n}.software'),
                        [
                            "type" => "software",
                            "title" => "<h2>" . __d("Softwares", "Working well with {0}", $software->softwarename) . "</h2>",
                            "link" => [
                                "id" => $software->id,
                                "action" => "workswellSoftware",
                                "participate" => __d("Layout", " Add", $software->softwarename),
                            ],
//                            "linkParticipate" => [
//                                "id" => $software->id,
//                                "action" => "",
//                                "text" => __d("Softwares", "Add a software that works well with {0}.", $software->softwarename),
//                            ],
                            "indicator" => [
                                "idTooltip" => "softwaresWorkingWellWithListId",
                                "indicatorMessage" => __d("Softwares","softwares.softwaresWorkingWellWithList",$software->softwarename)
                            ],
                            "tooManyMsg" => __d("Layout", "See all ({0} softwares)",count(\Cake\Utility\Hash::extract($software->workswellsoftwares, '{n}.software'))),
                            "titleSeeAll" => __d("Softwares", "See all softwares working well with {0}", $software->softwarename),
                            "titleAddMore" => __d("Softwares", "Add a software that works well with {0}.", $software->softwarename),
                            "emptyMsg" => __d("Softwares", "There are no project for {0}", $software->softwarename)
                        ]
                    ); ?>
                </section>