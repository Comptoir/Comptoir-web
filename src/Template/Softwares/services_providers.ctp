<?php
/**
 * Created by IntelliJ IDEA.
 * User: mpastor
 * Date: 26/07/16
 * Time: 15:00
 */

?>

<?php
$this->layout('base');

$this->assign('title', __d("Softwares","Service providers for {0}", $software->softwarename));

?>



    <section>
<?php
echo $this->Lists->block(
    \Cake\Utility\Hash::extract($software->providerssoftwares, '{n}.user'),
    [
        "type" => "user",
        "title" => "<h2>" . __d("Softwares", "Service providers for {0}", $software->softwarename) . "</h2>",
        "link" => [
            "id" => $software->id,
            "action" => "servicesProviders",
            "participate" => __d("Layout", " Add"),
        ],
        "linkParticipate" => [
            "id" => $software->id,
            "action" => "addServicesProvider",
            "text" => __d("Softwares", "Softwares.Users.DelcareAs.serviceProvider.addMessage", $software->softwarename),
        ],
        "linkFallBack" => [
            "id" => $software->id,
            "action" => "fallBackServicesProvider",
            "text" => __d("Softwares", "Softwares.Users.DelcareAs.serviceProvider.removeMessage", $software->softwarename),
        ],
        "indicator" => [
            "idTooltip" => "serviceProvidersOfListId",
            "indicatorMessage" => __d("Softwares", "softwares.serviceProvidersOfList", $software->softwarename)
        ],
        "tooManyMsg" => __d("Layout", "See all ({0} services providers)", count(\Cake\Utility\Hash::extract($software->providerssoftwares, '{n}.user'))),
        "titleSeeAll" => __d("Softwares", "See all service providers of {0}", $software->softwarename),
        "titleAddMore" => __d("Softwares", "Softwares.Users.DelcareAs.serviceProvider.addMessage", $software->softwarename),
        "titleFallBack" => __d("Softwares", "Softwares.Users.DelcareAs.serviceProvider.removeMessage", $software->softwarename),
        "emptyMsg" => __d("Softwares", "No service provider for {0}.", $software->softwarename)
    ],
    false
); ?>
        </section>
