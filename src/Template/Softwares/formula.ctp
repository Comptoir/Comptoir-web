<?php

/**
 * Template for to show all reviews of a software
 * for the validator we use : "&lsaquo;" for "<"  and "&rsaquo;" for ">"
 */

$this->layout('base');

$this->assign('title', __d("Softwares", "Formula"));

?>
<div class="row">
    <div class="col-sm-12 col-xs-12  col-md-9 col-md-offset-1 well ">
        <h1>
            <?php echo __d("SoftwareFormula", "Formula:"); ?>
        </h1>

        <p>
            <?php echo __d("SoftwareFormula", "( Σ points obtenus pour les indicateurs automatiques + Σ points obtenus pour les indicateurs d'usage ) / Nombre de points maximal"); ?>
        </p>
    </div>
</div>

<h2>
    <?php echo __d("SoftwareFormula", "Formula's elements and descriptions"); ?>
</h2>

<h3>
    <?php echo __d("SoftwareFormula", "Componants of SourceMetric"); ?>
</h3>

<table class="table table-hover table-bordered table-responsive">

    <tbody>
    <?php foreach ($formula['metrics'] as $key => $value) : ?>
        <?php if (strpbrk($key, 'S')) : ?>

            <tr class="info">
                <td>
                    <?php echo __d("Softwares", str_replace("Sm_", "", $key)); ?>
                </td>
                <td>
                    <?php echo $value; ?>
                </td>
            </tr>

        <?php endif; ?>

    <?php endforeach; ?>
    </tbody>
</table>

            <span>
                <?php echo __d("SoftwareFormula", "Number of automatic metrics: "); ?><?php echo $formula['nbSource'] [0]; ?>
            </span>

<h3>
    <?php echo __d("SoftwareFormula", "Componants of ComptoirMetric"); ?>
</h3>

<table class="table table-hover table-bordered table-responsive">
    <tbody>
    <?php foreach ($formula['metrics'] as $key => $value) : ?>
        <?php if (strpbrk($key, 'C')) : ?>
            <tr class="success">
                <td>
                    <?php echo __d("Softwares", str_replace("Cm_", "", $key)); ?>
                </td>
                <td>
                    <?php echo $value; ?>
                </td>
            </tr>
        <?php endif; ?>

    <?php endforeach; ?>
    </tbody>
</table>

<h2>
    <?php echo __d("SoftwareFormula", "How the formula is calculated"); ?>
</h2>

<h3>
    <?php echo __d("SoftwareFormula", "Explaining the formula:"); ?>
</h3>
<br/>
<p> <?php echo __d("SoftwareFormula", "Each metrics has a grading scale that determines the amount of points awarded to the software."); ?></p>
<p> <?php echo __d("SoftwareFormula", "If we take the age of the project as a metrics, the grading scale is: "); ?></p>
<br/>
<table class="table table-hover table-bordered table-responsive">
    <tbody>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", 'If we have "project_age < 3 years" then the software does not gain any points.'); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", 'If we have "3 years < project_age < 5 years" then the software gains one out of two points.'); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", 'If we have "project_age > 5 years" then the software gains two out of two points.'); ?>
        </td>
    </tr>
    </tbody>
</table>
<br/>
<p> <?php echo __d("SoftwareFormula", "For the number of contributors, the number of commits in one month, and the number of commits in twelve months there is an extra step taken to calculate the score. Each metric is compared to the average of all softwares and multiplied by 100 to get a percentage. Here is the calculation for the number of contributors:"); ?></p>
<br/>
<div class="well ">
    <p><?php echo __d("SoftwareFormula", "( nombre de contributeurs - moyenne de nombre de contributeurs ) / moyenne de nombre de contributeurs"); ?> </p>
</div>
<br/>
<p> <?php echo __d("SoftwareFormula", "After we have applied the grading scale on each metric of the software, we find the sum of all the points. Then we divide the sum by the maximum amount of points a software can get and we multiply that by a hundred to get the final score."); ?></p>
<h3>
    <?php echo __d("SoftwareFormula", "Grading scales:"); ?>
</h3>
<br/>
<table class="table table-hover table-bordered table-responsive">
    <thead>
    <tr>
        <th><?php echo __d("SoftwareFormula", "Age of the last commit"); ?></th>
        <th><?php echo __d("SoftwareFormula", "Points"); ?></th>
    </tr>
    </thead>
    <tbody>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "age &lsaquo; 30 days"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "4 points"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "30 days &lsaquo; age &lsaquo; 90 days"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "3 points"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "90 days &lsaquo; age &lsaquo;  2 years"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "2 points"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "2 years &lsaquo; age &lsaquo; 3 years"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "1 point"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "age &rsaquo; 3 years"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "0 point"); ?>
        </td>
    </tr>
    </tbody>
</table>
<br/>
<table class="table table-hover table-bordered table-responsive">
    <thead>
    <tr>
        <th><?php echo __d("SoftwareFormula", "Age of the project"); ?></th>
        <th><?php echo __d("SoftwareFormula", "Points"); ?></th>
    </tr>
    </thead>
    <tbody>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "project_age &lsaquo; 3 years"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "0 point"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "3 years &lsaquo; project_age &lsaquo; 5 years"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "1 point"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "project_age &rsaquo; 5 years"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "2 points"); ?>
        </td>
    </tr>
    </tbody>
</table>
<br/>
<table class="table table-hover table-bordered table-responsive">
    <thead>
    <tr>
        <th><?php echo __d("SoftwareFormula", "delta_commit_1m"); ?></th>
        <th><?php echo __d("SoftwareFormula", "Points"); ?></th>
    </tr>
    </thead>
    <tbody>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "delta_commit_1m &rsaquo; 50%"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "4 points"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "50% &rsaquo; delta_commit_1m &rsaquo; 25%"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "3 points"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "25% &rsaquo; delta_commit_1m &rsaquo; 0%"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "2 points"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "0% &rsaquo; delta_commit_1m &rsaquo; -25%"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "1 point"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "-25% &rsaquo; delta_commit_1m"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "0 point"); ?>
        </td>
    </tr>
    </tbody>
</table>
<br/>
<table class="table table-hover table-bordered table-responsive">
    <thead>
    <tr>
        <th><?php echo __d("SoftwareFormula", "delta_commit_12m"); ?></th>
        <th><?php echo __d("SoftwareFormula", "Points"); ?></th>
    </tr>
    </thead>
    <tbody>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "delta_commit_12m &rsaquo; 50%"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "4 points"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "50% &rsaquo; delta_commit_12m &rsaquo; 25%"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "3 points"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "25% &rsaquo; delta_commit_12m &rsaquo; 0%"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "2 points"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "0% &rsaquo; delta_commit_12m &rsaquo; -25%"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "1 point"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "-25% &rsaquo; delta_commit_12m"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "0 point"); ?>
        </td>
    </tr>
    </tbody>
</table>
<br/>
<table class="table table-hover table-bordered table-responsive">
    <thead>
    <tr>
        <th><?php echo __d("SoftwareFormula", "delta_nb_contributors"); ?></th>
        <th><?php echo __d("SoftwareFormula", "Points"); ?></th>
    </tr>
    </thead>
    <tbody>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "delta_nb_contributors &rsaquo; 50%"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "4 points"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "50% &rsaquo; delta_nb_contributors &rsaquo; 25%"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "3 points"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "25% &rsaquo; delta_nb_contributors &rsaquo; 0%"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "2 points"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "0% &rsaquo; delta_nb_contributors &rsaquo; -25%"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "1 point"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "-25% &rsaquo; delta_nb_contributors"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "0 point"); ?>
        </td>
    </tr>
    </tbody>
</table>
<br/>
<table class="table table-hover table-bordered table-responsive">
    <thead>
    <tr>
        <th><?php echo __d("SoftwareFormula", "code_percent"); ?></th>
        <th><?php echo __d("SoftwareFormula", "Points"); ?></th>
    </tr>
    </thead>
    <tbody>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "code_percent &lsaquo; 10%"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "4 points"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "50% &rsaquo; code_percent &rsaquo; 10%"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "3 points"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "50% &rsaquo; code_percent &rsaquo; 90%"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "2 points"); ?>
        </td>
    </tr>
    <tr class="info">
        <td>
            <?php echo __d("SoftwareFormula", "90% &lsaquo; code_percent"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "0 point"); ?>
        </td>
    </tr>
    </tbody>
</table>
<br/>
<table class="table table-hover table-bordered table-responsive">
    <thead>
    <tr>
        <th><?php echo __d("SoftwareFormula", "Declared users"); ?></th>
        <th><?php echo __d("SoftwareFormula", "Points"); ?></th>
    </tr>
    </thead>
    <tbody>
    <tr class="success">
        <td>
            <?php echo __d("SoftwareFormula", "declared users &rsaquo; 5 users"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "2 points"); ?>
        </td>
    </tr>
    <tr class="success">
        <td>
            <?php echo __d("SoftwareFormula", "5 users &rsaquo; declared users &rsaquo; 0 users"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "1 point"); ?>
        </td>
    </tr>
    <tr class="success">
        <td>
            <?php echo __d("SoftwareFormula", "declared users = 0 users"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "0 point"); ?>
        </td>
    </tr>
    </tbody>
</table>
<br/>
<table class="table table-hover table-bordered table-responsive">
    <thead>
    <tr>
        <th><?php echo __d("SoftwareFormula", "average_review"); ?></th>
        <th><?php echo __d("SoftwareFormula", "Points"); ?></th>
    </tr>
    </thead>
    <tbody>
    <tr class="success">
        <td>
            <?php echo __d("SoftwareFormula", "average_review &rsaquo; 2.5 stars"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "2 points"); ?>
        </td>
    </tr>
    <tr class="success">
        <td>
            <?php echo __d("SoftwareFormula", "2.5 stars &rsaquo;= average_review &rsaquo; 0 stars"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "1 point"); ?>
        </td>
    </tr>
    <tr class="success">
        <td>
            <?php echo __d("SoftwareFormula", "average_review = 0 stars"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "0 point"); ?>
        </td>
    </tr>
    </tbody>
</table>
<br/>
<table class="table table-hover table-bordered table-responsive">
    <thead>
    <tr>
        <th><?php echo __d("SoftwareFormula", "nb_screenshots"); ?></th>
        <th><?php echo __d("SoftwareFormula", "Points"); ?></th>
    </tr>
    </thead>
    <tbody>
    <tr class="success">
        <td>
            <?php echo __d("SoftwareFormula", "nb_screenshots &rsaquo; 2 screenshots"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "2 points"); ?>
        </td>
    </tr>
    <tr class="success">
        <td>
            <?php echo __d("SoftwareFormula", "2 screenshots &rsaquo;= nb_screenshots &rsaquo; 0 screenshots"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "1 point"); ?>
        </td>
    </tr>
    <tr class="success">
        <td>
            <?php echo __d("SoftwareFormula", "nb_screenshots = 0 screenshots"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "0 point"); ?>
        </td>
    </tr>
    </tbody>
</table>
<br/>
<table class="table table-hover table-bordered table-responsive">
    <thead>
    <tr>
        <th><?php echo __d("SoftwareFormula", "label_code_gouv"); ?></th>
        <th><?php echo __d("SoftwareFormula", "Points"); ?></th>
    </tr>
    </thead>
    <tbody>
    <tr class="success">
        <td>
            <?php echo __d("SoftwareFormula", "Does have label_code_gouv"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "1 point"); ?>
        </td>
    </tr>
    <tr class="success">
        <td>
            <?php echo __d("SoftwareFormula", "Does not have label_code_gouv"); ?>
        </td>
        <td>
            <?php echo __d("SoftwareFormula", "0 point"); ?>
        </td>
    </tr>
    </tbody>
</table>