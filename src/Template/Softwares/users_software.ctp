<?php
/**
 * Created by IntelliJ IDEA.
 * User: mpastor
 * Date: 26/07/16
 * Time: 15:00
 */

$this->assign('title', __d("Softwares", "Users of {0}", $software->softwarename));

?>

<?php
$this->layout('base');
?>


<section>
    <?php echo $this->Lists->block(
        \Cake\Utility\Hash::extract($software->userssoftwares, '{n}.user'),
        [
            "type" => "user",
            "title" =>"<h1>" .  __d("Softwares", "Users of {0}", $software->softwarename) . "</h1>",
            "link" => [
                "id" => $software->id,
                "action" => "usersSoftware",
                "participate" => __d("Softwares", "Softwares.Users.DelcareAs.user.addMessage", $software->softwarename),
            ],
            "linkParticipate" => [
                "id" => $software->id,
                "action" => "addUserOf",
                "text" => __d("Softwares", "Softwares.Users.DelcareAs.user.addMessage", $software->softwarename),
            ],
            "linkFallBack" => [
                "id" => $software->id,
                "action" => "fallBackusersSoftware",
                "text" => __d("Softwares", "Softwares.Users.DelcareAs.user.removeMessage", $software->softwarename),
            ],
            "titleAddMore" => "",
            "titleFallBack" => __d("Softwares", "Softwares.Users.DelcareAs.user.removeMessage", $software->softwarename),
        ],
        false
    ); ?>

</section>
