<?php
/**
 * Created by IntelliJ IDEA.
 * User: mpastor
 * Date: 26/07/16
 * Time: 15:00
 */

$this->assign('title', __d("Softwares", "Alternative to {0}", $software->softwarename));

?>

<?php
$this->layout('base');
?>



<section>

    <?php echo $this->Lists->block(
        \Cake\Utility\Hash::extract($software->alternativeto, '{n}.software'),
        [
            "type" => "software",
            "title" => "<h1>" . __d("Softwares", "Alternative to {0}", $software->softwarename) . "</h1>",
            "link" => [
                "id" => $software->id,
                "action" => "alternativeTo",
                "participate" => __d("Softwares", "Declare a software that is an alternative to {0}.", $software->softwarename),
            ],
            "titleAddMore" => __d("Softwares", "Add a software that works well with {0}.", $software->softwarename),
        ],
        false
    ); ?>

</section>
