<?php

$this->layout = 'base';
$this->assign('title', __d("Softwares",'Softwares.index.title'));

?>

<?php if (!empty( $softwares ) ): ?>
    <section>
        <?php
        echo $this->Lists->block(
            $softwares,
            [
                "type" => "software",
                "link" => [
                    "action" => "add",
                    "participate" => __d("Softwares", "Add a software"),
                ],
                "linkParticipate" => [
                    "id" => null,
                    "action" => "add",
                    "text" => __d("Softwares", "Add a software"),
                    "authorized" => true,
                ],
                "form" => $this->element("Pages/SearchFormSoftwares"),
                "title" => "<h1>" . __d("Softwares","Software list for Comptoir du libre ({0})",count($softwares)) . "</h1>",
                "titleAddMore" => __d("Softwares", "Add a software in Comptoir du libre"),
                "emptyMsg" => __d("Softwares", "No software in {0}, yet.", "Comptoir du libre"),

            ],
            false
        ); ?>

    </section>

<?php endif; ?>
