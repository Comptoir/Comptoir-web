<?php
$this->layout("base");
$this->assign('title', __d("Forms",'Edit ' . $software->softwarename, isset($message) ? " - ".$message : ""));
?>

<div class="row">
    <div class = "warning-form bg-warning col-xs-offset-3 col-xs-6">
        <?= __d("Forms","Fields marked with an asterisk ({0}) are required.",'<span class = "asterisk">*</span>'); ?>
    </div>
</div>

<div class="row">
    <div class="reviews form col-xs-offset-3 col-xs-6 columns content">
        <div class="softwares form large-9 medium-8 columns content">
            <?= $this->FormComptoir->create(null, ['type' => 'file',"enctype" => "multipart/form-data", 'url' => ['controller' => 'Softwares', 'action' => 'edit',isset($software->id)? $software->id :null]]) ?>
            <fieldset>
                <legend><?= "<h1>" .  __d("Forms",'Edit {0}' ,$software->softwarename)."</h1>"  ?></legend>

                <div  <?= isset($errors->screenshots) ? 'class="form-group has-error "' : 'class="form-group"'?>>
                    <label class="control-label" for="screenshots.photo"><?= __d("Forms", " {0} Screenshots: ", '<span class = "asterisk">*</span>') ?></label>
                    <?= $this->FormComptoir->input(
                        'screenshots.photo',
                        [
                            'type' => 'file',
                            "label" => false,
                            "error"=> isset($errors->screenshots) ? (array)$errors->screenshots: null,
                        ]) ?>

                </div>
                    <?php $helpScreenshot = '<div class="help-block">
                            <ul>';
                        !isset($software->screenshots->file) ?  $helpScreenshot .=  '<li>'.__d("Forms","Accepted formats JPEG, PNG, GIF, SVG.").'</li>' : "";
                        !isset($software->screenshots->fileBelowMaxWidth) || !isset($software->screenshots->fileBelowMaxHeigth) ?  $helpScreenshot .=  '<li>'.__d("Forms","Maximum size: 1920x1080px.").'</li>' : "";
                        !isset($software->screenshots->fileBelowMaxSize) ? $helpScreenshot .=  '<li>'.__d("Forms","Maximum weight: 1{0}.",__d('Forms',"<abbr title='Megabyte'>Mb</abbr>")).'</li>' : "";
                        $helpScreenshot .='</ul>
                            </div>';
                        echo $helpScreenshot;
                    ?>
                <?php echo $this->Form->input('screenshots.url_directory', ['type' => 'hidden']); ?>
                <div <?= isset($errors->licence) ? 'class="form-group has-error "' : 'class="form-group"' ?>>
                    <label class="control-label" for="licence_id"><?= __d("Forms", " {0} Licenses: ", '<span class = "asterisk">*</span>') ?></label>
                    <?= $this->FormComptoir->input(
                        'licence_id',
                        ['options' => $licenses,
                            "value" => isset($software->licence_id) ? $software->licence_id: null,

                            'empty' => false,
                            "class" => "form-control",
                            "label" => false,
                            "errors" => isset($errors->licence) ? $software->licence : null,
                        ]) ?>
                </div>
            </fieldset>
            <?= $this->Form->button(__d("Forms",'Submit'),["class"=>"btn btn-default addmore"]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>