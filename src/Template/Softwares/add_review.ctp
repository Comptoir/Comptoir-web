<?php
$this->layout("base");
$this->assign('title', __d("Forms",'Add a review{0}', isset($message) ? " - ".$message : ""));

?>



<div class="row">
    <div class = "warning-form bg-warning  col-xs-offset-3 col-xs-6">
        <?= __d("Forms","Fields marked with an asterisk ({0}) are required.",'<span class = "asterisk">*</span>'); ?>
    </div>
</div>

<div class="row">
    <div class="reviews form col-xs-offset-3 col-xs-6 columns content">
            <?= $this->FormComptoir->create(null,["enctype"=>"multipart/form-data",'url' => ['controller' => 'Softwares', 'action' => 'addReview']]) ?>
        <fieldset>
            <legend><?= __d("Forms",'Add Review') ?></legend>

            <div  <?= isset($review->title->_empty) ? 'class="form-group has-error "' : 'class="form-group"'?>>
            <label  class = "control-label" for="title"><?= __d("Forms"," {0} Title: ", '<span class = "asterisk">*</span>')?></label>
                <?= $this->FormComptoir->input(
                    'title',
                    [
                        'label'=>false,
                        "class"=>"form-control",
                        "required"=>"required",
                        "error"=>(isset($review->title->_empty)) ? (array)$review->title->_empty: null,
                    ]); ?>
            </div>
            <div  <?= isset($review->comment->_empty) ? 'class="form-group has-error "' : 'class="form-group"'?>>
                <label  class = "control-label" for="comment"><?= __d("Forms"," {0} Comment: ", '<span class = "asterisk">*</span>')?></label>
                <?= $this->FormComptoir->input("comment",
                    [
                        'label'=>false,
                        "class"=>"form-control",
                        "required"=>"required",
                        "type"=>"textarea",
                        "error"=>(isset($review->comment->_empty)) ? (array)$review->comment->_empty: null,
                    ]); ?>
            </div>
            <div class='form-group '>
                <label  class = "control-label" for="evaluation"><?= __d("Forms"," {0} Evaluation: ", '<span class = "asterisk">*</span>')?></label>

                <select name = "evaluation" class="form-control">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                </select>
            </div>
            <div class='form-group'>

                <?=  $this->FormComptoir->input('software_id',
                    [
                        'value' => isset($softwareId) ? $softwareId : null,
                        'empty' => true,
                        "type"=>"hidden",
                        "required"=>"required",

                    ]);?>
            </div>
        </fieldset>
        <?= $this->FormComptoir->button(__d("Forms",'Submit'),["class"=>"btn btn-default addmore"]) ?>
        <?= $this->FormComptoir->end() ?>
    </div>
</div>
