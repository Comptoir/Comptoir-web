<?php

$this->assign('title', __d("Softwares", "Review of {0} for {1}", [$review->user->username, $review->software->softwarename]));
$this->layout('base');
use Cake\I18n\I18n;

?>

<section class="row clearfix">
    <?php echo $this->element("Softwares/overview", ["software" => $review->software]) ?>
</section>

<section class="row">
    <div class="col-md-offset-3 col-md-6">
        <div class="blockReview ">
            <div class="row">
                <div class="col-xs-3">
                    <p class="UserReview col-xs-12">
                        <?= $this->Html->link($review->user->username,
                            ['controller' => 'Users', 'action' => 'view', $review->user->id],
                            ['escape' => false, "title" => __d("Users", "User name : ") . $review->user->username]) ?></p>
                    <p class="col-xs-12">
                        <?= $this->Rating->display($review->evaluation) ?>
                    </p>
                    <p class="col-xs-12"><?= $this->Time->format(strtotime($review->created),
                            [\IntlDateFormatter::SHORT, -1],
                            I18n::locale()) ?></p>
                </div>

                <div class="col-xs-8 col-xs-offset-1">
                    <h3><?= $review->title ?></h3>
                    <p><?= $review->comment ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
