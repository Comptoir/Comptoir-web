<?php

$cakeDescription = __('Comptoir du libre');
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    
    <?= $this->Html->meta('icon') ?>

    <?=  $this->Html->css('bootstrap/css/bootstrap.css')?>
    <?=  $this->Html->css('bootstrap/css/3-col-portfolio.css')?>
    <?=  $this->Html->script(['bootstrap/js/jquery.js', 'bootstrap/js/bootstrap.js'])?>
    
    <?= $this->Html->script('results') ?>
    
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('Form.css') ?>
    <?= $this->Html->css('results.css') ?>
   
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    
    
    
    
</head>
<body  onload="javascript:timer(5);">
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-3 medium-4 columns">
            <li class="name">
                <h1><a href=""><?= __("Comptoir du libre") ?></a></h1>
            </li>
        </ul>
        
        <section class="top-bar-section">
            <ul class="right">
                <li><a target="_blank" href="#"><?= __("Documentation") ?></a></li>
                <li><a target="_blank" href="http://localhost/manivelle/api/v1/forgeList"><?= __("API") ?></a></li>
            </ul>
        </section>
    </nav>
    
    <section class="container clearfix large-12 medium-12 columns">
        <?= $this->fetch('content') ?>
    </section>
    <footer>
        <!-- TODO -->
    </footer>
</body>
</html>
