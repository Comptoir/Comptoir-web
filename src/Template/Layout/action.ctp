<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>


<!DOCTYPE html>
<html lang="en">
      <?= $this->element("Pages/header") ?>
    <body>

    <?= $this->element("Navigation/navbarFixedTop") ?>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-1 main">
                </div>   
            </div>         
        </div>
        <footer>
            <?= $this->element("Pages/footer")?>
        </footer>
    </body>
</html>
