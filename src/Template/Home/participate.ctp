<?php

$this->layout = 'base';
$this->assign('title', __d("Home", "How to participate"));

?>

<h1><?= __d("Home", "How to participate") ?></h1>

<p>
    <?= __d("Home", "If you want to contribute now you can follow the instructions on {0}.",
        $this->Html->link("Premiers pas sur le Comptoir du libre",
            "http://faq.adullact.org/general/106-premiers-pas-sur-le-comptoir-du-libre",
            ['escape' => false]) ) ?>
</p>
<p>
    <?= __d("Home", "Here is our email address :") ?> <a href="mailto:comptoir@adullact.org">comptoir@adullact.org</a>
</p>