<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->layout = 'base';
$this->assign('title', __d("Home", "Contact the Comptoir"));

?>
<h1><?= __d("Home", "Contact the Comptoir") ?></h1>
<ul class="list-unstyled">
    <li>
        <i class="glyphicon glyphicon-user"></i> Matthieu FAURE<?= __d("Home", " : Project leader") ?>
    </li>
    <li>
        <i class="glyphicon glyphicon-phone-alt"></i><?= __d("Home", "  Telephone : 04 67 65 05 88 ") ?>
    </li>
    <li>
        <i class="glyphicon glyphicon-envelope"></i><?= __d("Home", " Courriel : ") ?><a
            href="mailto:comptoir@adullact.org">comptoir@adullact.org</a>
    </li>
    <li>
        <i class="glyphicon glyphicon-map-marker"></i><?= __d("Home", " Address : ") ?> Association Adullact, 836 rue du
        Mas de Verchant, 34000 Montpellier
    </li>
</ul>