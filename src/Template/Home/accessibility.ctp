<?php

$this->layout = 'base';
$this->assign('title', __d("Home", "Accessibility"));

?>

<h1><?= __d("Home", "Accessibility") ?></h1>

<p>
   <?=
    __d("Home",
        "Accessibility conformance is in progress (<abbr title=\"Web Content Accessibility Guidelines\">WCAG</abbr> / <span lang=\"fr\"><abbr title=\"Référentiel Général d'Accessibilité des Administrations\">RGAA</abbr></span>)")
    ?>
</p>