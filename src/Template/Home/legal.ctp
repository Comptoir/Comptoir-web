<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->layout = 'base';
$this->assign('title', __d("Home", "Legal"));

?>
<h1><?= __d("Home", "Legal") ?></h1>

<h2> <?= __d("Home", "Postal Adress") ?> </h2>

    <p>
        836, rue du mas de Verchant - Bât. Le Tucano - 34000 MONTPELLIER
    </p>
<h3>
    <?= __d("Home","Editor") ?>
</h3>

     <p>
         ADULLACT
     </p>

<h3>
    <?= __d("Home","Publication director") ?>
</h3>
     <p>
         <?= __d("Home","Pascal Kuczynski, General Director of ADULLACT") ?>
     </p>

<h3><?= __d("Home","Hosting") ?></h3>
     <p>OVH : 2 rue Kellermann - 59100 Roubaix - France.</p>
</p>

<h3><?= __d("Home", "Contact") ?></h3>
    <p>
        <?= __d("Home", "Email address :") ?>

        <a href="mailto:comptoir@adullact.org">comptoir@adullact.org</a>
    </p>

