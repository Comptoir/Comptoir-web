<?php
$this->layout = 'base';
$this->assign('title', __d("Home", "Comptoir du libre"));
$this->assign('header', $this->element("Home/header"));
?>

    <div class="row">
        <div class="jumbotron-cdl col-xs-12">
            <h2><?= __d("Pages","Home.message.title") ?></h2>
            <p>
                <?= __d("Pages", "Home.message.content.numbers", [count($softwares), count($reviews), count($users)]) ?>
                <br/>
                <?= __d("Pages", "Home.message.content.phrase") ?>
            </p>
        </div>
    </div>

<?php if (isset($softwaresSelected)) : ?>
    <?php
    echo $this->Lists->block(
        \Cake\Utility\Hash::extract($softwaresSelected->softwares, '{n}'),
        [
            "type" => "software",
            "title" => "<h2>" . __d("Home", "Pick of the month") . "</h2>",
            "link" => [
                "participate" => "",
            ],
            "tooManyMsg" => "",
            "titleSeeAll" => "",
            "titleAddMore" => "",
            "emptyMsg" => "",
        ],
        true
    ); ?>


<?php endif; ?>


<?php if (isset($lastAdded)) : ?>
    <?php echo $this->Lists->block(
        \Cake\Utility\Hash::extract($lastAdded->softwares, '{n}'),
        [
            "type" => "software",
            "title" => "<h2>" . __d("Home", "Last added") . "</h2> ",
            "link" => [
                "participate" => "",
            ],
            "tooManyMsg" => "",
            "titleSeeAll" => "",
            "titleAddMore" => "",
            "emptyMsg" => "",
        ],
        true
    ); ?>
<?php endif; ?>