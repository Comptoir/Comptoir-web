<?php

// Dans un controller

namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;

use Cake\Network\Http\Client;
use Cake\View\Form\FormContext;
use Cake\Utility\Hash;

/**
 * CakePHP UsersController
 * @author mpastor
 */
class UsersController extends AppController {

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('ComptoirServer');

        $linkParticipate =[];
        !$this->request->session()->read('Auth.User.username') ?
            $linkParticipate =  [
                "linkParticipate" => [
                    "id" => null,
                    "action" => "add",
                    "text" => __d("Forms", "Sign up"),
                    "authorized" => true,
                ]
            ] : null
        ;

        $this->set("linkParticipate",$linkParticipate);
    }


    public function beforeFilter(\Cake\Event\Event $event)
    {
        $this->Auth->allow([
            'add',
            'providers',
            'Backedsoftwares',
            'usedSoftwares',
            'Createdsoftwares',
            'contributionsSoftwares',
            'providerforSoftwares',
            'administrations',
            'nonProfitUsers',
            'lostPassword']);

        $this->Auth->deny(['edit']);
        parent::beforeFilter($event);

    }


    public function login (){
        if ($this->request->is('post')) {
            try {
                $http = new Client();
                $response = $http->post( COMPTOIR_SRV_API."users/register.json",
                    [],
                    [
                        'auth' => [
                            'type' => 'digest',
                            'username' => $this->request->data['username'],
                            'password' => $this->request->data['password']
                        ],
                        'type' => 'json'
                    ]);
                $body = json_decode($response->body);

                if (isset($body->digest)){
                    $this->Flash->success(__d("Forms","You are logged"));

                    $this->Cookie->config('Digest', 'path', '/');
                    $this->Cookie->configKey('Digest', [
                        'expires' => '+1 days',
                        'httpOnly' => true
                    ]);
                    $this->Cookie->config('User', 'path', '/');
                    $this->Cookie->configKey('User', [
                        'expires' => '+1 days',
                        'httpOnly' => true
                    ]);

                    $this->Cookie->write( 'Digest',json_decode(json_encode($body),true) );
                    $this->Cookie->write( 'User',['password' => $this->request->data['password']]);
                    $this->Auth->setUser([
                        'username' => $body->username,
                        'user_type' => $body->userType,
                        'password' => $this->request->data['password'],
                        'id' => $body->id,
                    ]);
                    return $this->redirect(["controller"=>"Home","action"=>"index", "language"=>$this->request->param("language")]);
                }else{
                    $this->Flash->error(__d("Forms","You are not logged"));
                    $this->set("message","Error");
                    $this->set("user", ["error"=>__d("Forms","The login or password given is invalid.")]);
                }
            }catch (Exception $exception){
                $this->log(var_export($exception,true),"debug");
                return false;
            }
        }
    }

    public function logout()
    {
        $this->Cookie->delete('User');
        $this->Cookie->delete('Digest');
        $this->request->session()->destroy();
        if (!($this->Cookie->check('User') && $this->Cookie->check('Digest'))){
            $this->Auth->logout();
            return $this->redirect(["controller"=>"Home","action"=>"index", "language"=>$this->request->param("language")]);        }
    }

    public function changePassword($id = null){

        if (!empty($this->request->data)){
            $result =  $this->ComptoirServer->put(COMPTOIR_SRV_API . "users/changePassword/" . $id , 'item', 'user',$this->request->data);
        }else{
            $result = $this->ComptoirServer->get(COMPTOIR_SRV_API . "users/changePassword/". $id , 'user', 'userTypes');
        }
        $this->set("rules", Hash::get((array)$result, "rules"));
        $this->set("userTypes", Hash::get((array)$result, "userTypes"));
        $this->set("user", Hash::get((array)$result, "user"));
        $this->set("id",$id);

        if (Hash::get((array)$result, "message") === "Success"){
            $this->Cookie->write( 'User',['password' => $this->request->data['new_password']]);
            $this->Flash->success(__d("Forms","Your password has been changed."));
            return $this->redirect(["controller"=>"Users","action"=>"login", "language"=>$this->request->param("language"),$id]);

        }
        elseif (Hash::get((array)$result, "message") === "Error"){
            return $this->Flash->error(__d("Forms","Your password can not be changed, please follow rules in red."));
        }

    }

    /**
     * @param null $id
     */
    public function lostPassword($token=null){
        if (!empty($this->request->data) && !$token){

            $result =  $this->ComptoirServer->post(COMPTOIR_SRV_API . "users/forgotPassword", 'item', 'user',$this->request->data);
            $message = Hash::get((array)$result, "message");
            $this->set("message", Hash::get((array)$result, "message"));
            $message =="" ?
                $this->Flash->success(__d("Forms","A link to reset your password will be sent on the specified email adress.")) :
                $this->Flash->error(__d("Forms","We can not give you a new passord, pelase contact us at comptoir@adullact.org."));
        }
        elseif($token){
            $this->viewBuilder()->template("reset_password");
            if (!empty($this->request->data)) {
                $result = $this->ComptoirServer->put(COMPTOIR_SRV_API . "users/changePassword/" . $token, "user", "message", $this->request->data);
                if (Hash::get((array)$result, "message") === "Success"){
                    $this->Cookie->write( 'User',['password' => $this->request->data['new_password']]);
                    $this->Flash->success(__d("Forms","Your password has been changed."));
                    return $this->redirect(["controller"=>"Users","action"=>"login", "language"=>$this->request->param("language")]);

                }
                elseif (Hash::get((array)$result, "message") === ""){
                    $this->Flash->error(__d("Forms","Your password can not be changed"));
                }
            }

        }
        $this->set("id",$token);

    }

    public function view($id=null) {
        $result = $this->ComptoirServer->get(COMPTOIR_SRV_API . "users/" . $id, 'user', 'user');

        $user = Hash::get((array)$result, "user");

        //For Social MEDIAS => OPENGRAPH
        $openGraph = [
            "title"  => $user->username,
            "description" => $user->description,
            "image" => COMPTOIR_SRV_URL . $user->logo_directory . DS . $user->photo,
            "imageSize" => ['width' => '200', 'height' => '300']
        ];

        $card = [
            "title"  => $user->username,
            "description" => $user->description,
            "image" => COMPTOIR_SRV_URL . $user->logo_directory . DS . $user->photo,
            "imageSize" => ['width' => '200', 'height' => '300']
        ];

        $this->set('openGraph', $openGraph);
        $this->set('card', $card);
    }

    /**
     * To add a new user in database
     */
    public function add (){



        if (!empty($this->request->data)){
            $result =  $this->ComptoirServer->post(COMPTOIR_SRV_API . "users/" , 'item', 'user',$this->request->data);
        }else{
            $result = $this->ComptoirServer->get(COMPTOIR_SRV_API . "users/add", 'user', 'userTypes');
        }

        $this->set("rules", Hash::get((array)$result, "rules"));
        $userTypes = (array)Hash::get((array)$result, "userTypes");
        $this->set("userTypes",$userTypes);
        $this->set("user", Hash::get((array)$result, "user"));
        $this->set("message", Hash::get((array)$result, "message"));

        $this->set("errors", Hash::get((array)$result, "errors"));

        if (Hash::get((array)$result, "message") === "Success"){
            $this->Flash->success(__d("Forms","Your are registred on the Comptoir du Libre, welcome !"));
            return $this->redirect(["controller"=>"Home","action"=>"index", "language"=>$this->request->param("language")]);
        }
        elseif (Hash::get((array)$result, "message") === "Error"){
            return $this->Flash->error(__d("Forms","Your registration failed, please follow rules in red."));
        }

    }
    /**
     * To edit an user in database
     */
    public function edit ($id = null ){



        if (!empty($this->request->data)){
            $result =  $this->ComptoirServer->put(COMPTOIR_SRV_API . "users/" . $id , 'item', 'user',$this->request->data);
        }else{
            $result = $this->ComptoirServer->get(COMPTOIR_SRV_API . "users/edit/". $id , 'user', 'userTypes');
        }

        $this->set("rules", Hash::get((array)$result, "rules"));
        $this->set("userTypes", Hash::get((array)$result, "userTypes"));
        $this->set("user", Hash::get((array)$result, "user"));
        $this->set("errors", Hash::get((array)$result, "errors"));

        $this->set("id",$id);

        if (Hash::get((array)$result, "message") === "Success"){
            $this->Flash->success(__d("Forms","Edit.User.profile.success"));
        }
        elseif (Hash::get((array)$result, "message") === "Error"){
            $this->Flash->error(__d("Forms","Edit.User.profile.error"));
        }

    }

    public function addReview () {

        if (!empty($this->request->data)){
            $result = $this->ComptoirServer->post(COMPTOIR_SRV_API . "users/add/" , 'user', 'user',$this->request->data);
        }else{
            $result = $this->ComptoirServer->get(COMPTOIR_SRV_API . "users/add/", 'user', 'userTypes');
        }


    }

    public function index() {
        $this->ComptoirServer->get(COMPTOIR_SRV_API . "users/", 'users', 'users');
    }

    public function providers() {

        $this->viewBuilder()->template("users_list");
        $this->set("title","List of service providers");
        $this->ComptoirServer->get(COMPTOIR_SRV_API . "users/servicesproviderUsers/", 'users', 'users');

    }
    public function administrations() {
        $this->viewBuilder()->template("users_list");
        $this->set("title","List of administrations");
        $this->ComptoirServer->get(COMPTOIR_SRV_API . "users/administrations/", 'users', 'users');
    }
    public function nonProfitUsers() {

        $this->viewBuilder()->template("users_list");
        $this->set("title","List of non profit users");
        $this->ComptoirServer->get(COMPTOIR_SRV_API . "users/nonProfitUsers/", 'users', 'users');
    }

    /**
     * Get a list of softwares create by a specific user
     */
    public function creatorOf() {


        try {
            $http = new Client();

            $response = $http->get(COMPTOIR_SRV_API . "relationships-softwares-users/getcreatorOf", $this->request->query, ['type' => 'json']);

            $users = $response->body('json_decode');
        } catch (Exception $exception) {
            $this->log(var_export($exception, true), "debug");
        }
        $this->set("users", $users);
    }

    public function Backedsoftwares ( $id=null ){
        $this->ComptoirServer->get(COMPTOIR_SRV_API . "users/backedSoftwares/" . $id, 'user', 'user');

    }

    public function usedSoftwares ( $id = null ){
        $this->ComptoirServer->get(COMPTOIR_SRV_API . "users/usedSoftwares/" . $id, 'user', 'user');

    }

    public function Createdsoftwares ( $id=null ){

        $this->ComptoirServer->get(COMPTOIR_SRV_API . "users/createdSoftwares/" . $id, 'user', 'user');
    }

    public function contributionsSoftwares ( $id=null ){
        $this->ComptoirServer->get(COMPTOIR_SRV_API . "users/contributionsSoftwares/" . $id, 'user', 'user');
    }

    public function providerforSoftwares ( $id=null ){
        $this->ComptoirServer->get(COMPTOIR_SRV_API . "users/providerforSoftwares/" . $id, 'user', 'user');
    }

}
