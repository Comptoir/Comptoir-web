<?php

// Dans un controller
namespace App\Controller;

use App\Controller\AppController;
use App\Form\RequestForm;


class RequestController extends AppController
{
    public function index()
    {
        $request = new RequestForm();
        $this->set('carousel_top',$request->carousel_top );
        if ($this->request->is('post')) {
            if ($request->execute($this->request->data)) {
                $this->set('json_datas',$request->getDatas($this->request->data));
                $this->set('forgesDatas',$request->getForgesDatas());
                       

                //$this->Flash->success('Recherche envoyées avec succès.');
            } else {
                //$this->Flash->error('Il y a eu un problème lors de la soumission de votre formulaire.');
            }
        }
        $this->set('request', $request);
    }
    
    public function softwaresList()
    {

        $request = new RequestForm();
        if ($this->request->is('post')) {
            if ($request->execute($this->request->data)) {
                $softwares = json_decode($request->getDatas($this->request->data));
                $this->set('softwares',$softwares);
            } else {

            }
        }
    }
    
    public function request()
    {
        $request = new RequestForm();
        $this->set('carousel_top',$request->carousel_top );

        if ($this->request->is('post')) {
            if ($request->execute($this->request->data)) {
                $this->set('json_datas',$request->getDatas($this->request->data));
                $this->set('forgesDatas',$request->getForgesDatas());
                       

                //$this->Flash->success('Recherche envoyées avec succès.');
            } else {
                //$this->Flash->error('Il y a eu un problème lors de la soumission de votre formulaire.');
            }
        }
        $this->set('request', $request);
    }
}