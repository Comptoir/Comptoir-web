<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Form\RequestForm;
use Cake\Core\Configure;
use Cake\Network\Http\Client;

/**
 * Manage the home page
 */
class HomeController extends AppController {


    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('ComptoirServer');
    }


    public function index() {
        /**
         * Static array containing paremters for the request on manivelle server for spotlight on => carousel elements
         * This is a temporary method.
         */
//        $form = ['name' => '',
//                'limit' => '4',
//                'offset' => '0'];
//        $request = new RequestForm();
//        $this->set('carousel_top',Configure::read("CAROUSEL_FOCUS_TOP_APPS") );
//        $json = $request->getDatas($form);
//        $softwares = json_decode($json);
//
//      // ////////////////////////////
// SUPPRIMMER APRES LE CONGRES
///////////////////////////////
        try {
            $http = new Client();

            $response = $http->get(COMPTOIR_SRV_API . "softwares/getProjectsById", [27, 15, 9, 23], ['type' => 'json']);

            $responseLastAdded = $http->get(COMPTOIR_SRV_API . "softwares/lastAdded", [], ['type' => 'json']);

            $softwaresSelected = $response->body('json_decode');
            $softwaresLastAdded = $responseLastAdded->body('json_decode');

            $result =$this->ComptoirServer->get(COMPTOIR_SRV_API . "reviews/", 'reviews','reviews');
            $this->ComptoirServer->get(COMPTOIR_SRV_API . "users/", 'users', 'users');
            $this->ComptoirServer->get(COMPTOIR_SRV_API . "softwares/", 'softwares','softwares');

            $datas = (json_decode($response->body, true));
            $this->request->params['paging'] = [];


            $this->set('softwaresSelected', $softwaresSelected);
            $this->set('lastAdded', $softwaresLastAdded);
        } catch (Exception $exception) {
            $this->log(var_export($exception, true), "debug");
        }

        //For Social MEDIAS => OPENGRAPH
        $openGraph = [
            "title"  => \Cake\Core\Configure::read("OpenGraph.title"),
            "description" => \Cake\Core\Configure::read("OpenGraph.description"),
            "image" => \Cake\Core\Configure::read("OpenGraph.image"),
            "imageSize" => ['width' => '200', 'height' => '300']
        ];

        $card = [
            "title"  => \Cake\Core\Configure::read("OpenGraph.title"),
            "description" => \Cake\Core\Configure::read("OpenGraph.description"),
            "image" => \Cake\Core\Configure::read("OpenGraph.image"),
            "imageSize" => ['width' => '200', 'height' => '300']
        ];

        $this->set('openGraph', $openGraph);
        $this->set('card', $card);



//////////////////////////////////////////////:
//////////////////////////////////////////////:
//        $this->set('softwares',$softwares);
//$this->set('_serialize', ['softwares']);
//        $this->set('forgesDatas',$request->getForgesDatas());
    }

    public function contact() {

    }

    public function participate() {

    }

    public function legal() {

    }

    public function accessibility() {

    }



}
