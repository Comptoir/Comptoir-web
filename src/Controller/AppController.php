<?php


namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

use Cake\I18n\I18n;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{


    protected $availableLanguages = [
        'en' => 'en',
        'fr' => 'fr',
    ];

    protected $explicitLanguages = [
        'en' => 'english',
        'fr' => 'français',
    ];

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash', ['clear' => true]);
        $this->loadComponent('Cookie');

        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'Home',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Home',
                'action' => 'index',
            ]
        ]);

    }

    public function isAuthorized($user)
    {
        if (!$this->Auth->identify()) {
            return false;
        }

        return false;
    }


    public function beforeFilter(\Cake\Event\Event $event)
    {
        $this->Auth->allow(['index', 'view', 'logout', 'participate', 'contact', 'legal', 'accessibility', 'search']);

        $this->setLocale();
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    /**
     * Sets the current locale based on url param and available languages
     *
     * @return void
     */
    protected function setLocale()
    {
        $selectedLanguage = I18n::locale();
        $lang = $this->request->param('language') ? $this->request->param('language') : preg_replace('/_\w*/',"",I18n::locale());

        if ($lang && isset($this->availableLanguages[$lang])) {
            I18n::locale($lang);
            $selectedLanguage = $this->availableLanguages[$lang];
        }
        $this->set('selectedLanguage', $selectedLanguage);
        $this->set('availableLanguages', $this->availableLanguages);
        $this->set('explicitLanguages', $this->explicitLanguages);

    }
}
