<?php

namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Network\Http\Client;
use Cake\Core\Configure;

/**
 * Form to get datas
 */
class RequestForm extends Form
{
    /**
     * Number of elements in the Carousel (Spotlight on ...).
     * @var INT 
     */
    public $carousel_top ;
    
    
    public function __construct(){ 
        $this->carousel_top = Configure::read("CAROUSEL_FOCUS_TOP_APPS");
    }
    
    protected function _buildSchema(Schema $schema)
    {
        $schema->addField('name', 'string');
        $schema->addField('q', 'string');
        //$schema->addField('order', 'string');
        $schema->addField('limit', 'integer');
        $schema->addField('offset', 'integer');
        return $schema;
    }

    

    protected function _execute(array $data)
    {
        
    
        return true;
    }
    
    /**
     * Get a list of projects from Manivelle server thanks to the API.
     * @param array $data Represents all parameters for the request.
     * @return Json Returns all objects get thanks to the API as Json format. 
     */
    public function getDatas (array $data){

        try {
            $http = new Client();
            $response = $http->post(
                COMPTOIR_SRV_API."Pages/search",
                $data,
                ['type' => 'json']
            );
        return $response->body;

        }catch (Exception $exception){
            $this->log(var_export($exception,true),"debug");
        }  
    }
    
    /**
     * Get a list of connectors witch contains all available attribute.
     * @return Json Returns all objects get thanks to the API as Json format. 
     */
    public function getForgesDatas (){

        try {
            $http = new Client();

            $response = $http->get(COMPTOIR_SRV_API."connectorsList");

        return $response->body('json_decode');  

        }catch (Exception $exception){
            $this->log(var_export($exception,true),"debug");
        }  
    }
}