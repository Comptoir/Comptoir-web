<?php

namespace App\View\Helper;

use Cake\Core\Configure;
use Cake\View\Helper;
use Cake\View\StringTemplateTrait;

class RatingHelper extends Helper
{
    use StringTemplateTrait;

    protected $_defaultConfig = [
        'templates' => [
            'stars' => '<span {{attrGlyph}}>
                            <span {{attrStar}}>{{content}}</span>
                        </span>',
            'suffix' => '<span>
                <span {{attrValue}}>
                 {{value}}           
                </span>
                <span>
                 {{message}}           
                </span>
            </span>',
        ]
    ];

    public $helpers = ['Html'];

    /**
     * @param $value
     * @param array $options
     * @return string
     */
    public function display ($value,$options = []) {

        empty($options["content"]) ? $options["content"] = '*' : $options["content"];
        $options["indicator"] = empty($options["indicator"]) ? null : $options["indicator"];

        $starsElements = is_int($value) ? $this->countingStars($value) : $this->noRate();



        $suffix = $this->suffix($value,$options["indicator"]);

        return $starsElements . " " . $suffix;
    }

    /**
     * @param $value
     * @return string
     */
    public function countingStars ($value){
        $starsElements = "";



        if (is_int($value)){
            for ($stars = 0; $stars < Configure::read("Softwares.Rating.maximum"); $stars++) {
                if ($stars < $value) {
                    $starsElements .= $this->formatTemplate('stars',
                        [
                            'attrGlyph' => $this->templater()->formatAttributes(["class" => "glyphicon glyphicon-star rating-star"]),
                            'attrStar' => $this->templater()->formatAttributes(["class" => "sr-only"]),
                            'content' => "*",
                        ]
                    );
                }else {
                    $starsElements .= $this->formatTemplate('stars',
                        [
                            'attrGlyph' => $this->templater()->formatAttributes(["class" => "glyphicon glyphicon-star-empty rating-star"]),
                            'attrStar' => $this->templater()->formatAttributes(["class" => "sr-only"]),
                            'content' => "*",
                        ]
                    );
                }
            }
        }

        return $starsElements;
    }

    /**
     * @return string
     */
    public function noRate(){
        $starsElements = "";

            for ($stars = 0; $stars < Configure::read("Softwares.Rating.maximum"); $stars++) {
                $starsElements .= $this->formatTemplate('stars',
                    [
                        'attrGlyph' => $this->templater()->formatAttributes(["class" => "glyphicon glyphicon-star-empty rating-star"]),
                        'attrStar' => $this->templater()->formatAttributes(["class" => "sr-only"]),
                        'content' => "*",
                    ]
                );
            }

        return $starsElements;
    }

    /**
     * @param string $value
     * @param string $message
     * @return string
     */
    public function suffix ($value = 'NA',$message = ""){


        return $this->formatTemplate('suffix',
            [
                'value' => $value,
                'attrValue' => $this->templater()->formatAttributes(["class" => "sr-only"]),
                'message' => $message
            ]
        );
    }
}