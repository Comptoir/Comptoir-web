<?php

namespace App\View\Helper;

use Cake\Core\Configure;
use Cake\View\Helper;
use Cake\View\StringTemplateTrait;
use Cake\I18n\I18n;
use Cake\Utility\Hash;

use App\View\Helper\ScoreHelper;

class ListsHelper extends Helper
{

    use StringTemplateTrait;

    protected $_defaultConfig = [
        'templates' => [
            'review' => '
                        <li {{reviewLi}}>
                           
                            <div {{attrsReview}}>
                                {{LinkReviewPage}}
                                <div class="row">
                                    <div {{AttrsBootstrap1}}>
                                        <p {{AttrsUserName}}>{{userName}}</p>
                                        <p>{{stars}}</p>
                                        <p>{{created}}</p>
                                    </div>
                                    <div {{AttrsBootstrap2}}>
                                        <h3>{{title}}</h3>
                                        <p>{{reviewComment}}</p>
                                    </div>
                                </div>
                            </div>
                        </li>'
            ,
            'stars' => '<span {{attrGlyph}}><span {{attrStar}}>{{content}}</span></span>',
            'software' =>
                '<li {{attrsColumn}}>
                    <div {{attrsBlock}}> 
                        {{header}}{{headerEnd}}
                        <div class="size-logo">
                            {{LinkImage}}
                        </div>
                        {{softwareName}}
                        <p {{attrsDescription}}>{{softwareDescription}}</p>
                        <div class="rating-unit">{{review_average}}</div>
                    </div>
                </li>',
            'user' => '<li {{attrsColumn}}>
                    <div {{attrsBlock}}>
                    <div {{CssBadge}}>
                        <div {{attrsBadge}}>
                            <span class="sr-only">{{UserType}}</span>
                        </div>
                    </div>
                        {{header}}
                        {{name}}
                        {{headerEnd}}
                        <div class="size-logo">
                            {{LinkImage}}
                        </div>
                    </div>
                </li>',
            'header' => '<div class="align">
                            {{sectionTitle}}
                            {{indicator}}
                            <span class="allSee" {{attribs}}>
                                {{link}} 
                            </span>
                            {{addMore}}
                        </div>',
            'searchForm' => '<div {{attrsform}}>
                                {{form}}
                            </div>',
            'indicator' => ' <i {{attribsIndicator}}></i>
                            <div {{attribsToolTip}} >{{indicatorMessage}}</div>',
            'headerScreenShots' => '<div class="align">
                            <span class="allSee" {{attribs}}>
                                {{link}} 
                            </span>
                            {{addMore}}
                        </div>',
            'addMore' => '{{link}}',
            'seeMore' => '<li {{attrsColumn}}><div {{attribs}}>
                            <p>
                             {{link}}
                            </p>
                        </div></li>',
        ]
    ];

    public $helpers = ['Html', 'Text', 'Time', 'Score', 'Rating'];

    public function software($software, $limit = false)
    {
        $result = null;
        if (!empty($software)) {

            if (empty ($software->logo_directory) || empty($software->photo)) {
                $logo = $this->Html->link($this->Html->image("logos/Softwarelogo_placeholder.jpg",
                    ["alt" => __d("Softwares", "Go to the {0}'s page", $software->softwarename), "class" => "img-responsive"]),
                    ['controller' => 'Softwares', 'action' => 'view', $software->id],
                    ['escape' => false, "title" => __d("Softwares", "Software name : ") . $software->softwarename]);
            } else {
                $logo = $this->Html->link($this->Html->image(COMPTOIR_SRV_URL . $software->logo_directory . DS . $software->photo,
                    ["alt" => $software->softwarename, "class" => "img-responsive"]),
                    ['controller' => 'Softwares', 'action' => 'view', $software->id],
                    ['escape' => false, "title" => __d("Softwares", "Software name : ") . $software->softwarename]);
            }

            $attrsColumn = $this->templater()->formatAttributes(['class' => 'col-xs-12 col-sm-6 col-md-3 col-lg-3']);
            $attrsBlock = $this->templater()->formatAttributes(['class' => 'software-unit-home backgroundUnit']);

            $result .= $this->formatTemplate('software', [
                'attrsColumn' => $attrsColumn,
                'attrsBlock' => $attrsBlock,
                'review_average' => $this->Rating->display($software->average_review),
                'header' => $limit == true && ($this->request->controller != "Home") ? '<h3 class = "size-title">' : '<h2 class = "size-title">',
                'headerEnd' => $limit ? "</h3>" : "</h2>",
                'softwareName' =>
                    $this->Html->link(
                        $this->Text->truncate($software->softwarename, 35, ['ellipsis' => '...', 'exact' => false]),
                        ['controller' => 'Softwares', 'action' => 'view', $software->id,],
                        ['escape' => false, 'title' => __d("Softwares", "Software name : ") . $software->softwarename]
                    ),
                'LinkImage' => $logo,
                'attrsDescription' => $this->templater()->formatAttributes(['class' => 'text-overflow project-description']),
                'softwareDescription' => $this->Text->truncate($software->description, 100, ['ellipsis' => '...', 'exact' => false]),
                ['class' => 'text-overflow project-description'],
            ]);
        }
        return null !== $result ? $result : null;
    }

    /**
     * @param $users
     * @param bool $limit : true -> 4 columns else 6 columns
     * @return null
     */
    public function user($user, $limit = false)
    {
        $result = null;
        if (!empty($user)) {
            if ((empty ($user->logo_directory) || empty($user->photo))) {
                $logo = $this->Html->link($this->Html->image("logos/User_placeholder.jpg",
                    ["alt" => __d("Users", "Go to the {0}'s page", $user->username), "class" => "img-responsive"]),
                    ['controller' => 'Users', 'action' => 'view', $user->id],
                    ['escape' => false]);
            } else {
                $logo = $this->Html->link($this->Html->image(COMPTOIR_SRV_URL . $user->logo_directory . DS . $user->photo,
                    ["alt" => __d("Users", "Go to the {0}'s page", $user->username), "class" => "img-responsive"]),
                    ['controller' => 'Users', 'action' => 'view', $user->id],
                    ['escape' => false]);
            }

            $attrsColumn = $this->templater()->formatAttributes(['class' => 'col-xs-12 col-sm-6 col-md-3 col-lg-3']);
            $attrsBlock = $this->templater()->formatAttributes(['class' => 'user-unit-home backgroundUnit list-unstyled']);

            /**
             * Determine badge (community, asso or company, ) between UsersBlock's categories.
             */
            if ($user->user_type->name == "Administration") {
                $attrsBadge = $this->templater()->formatAttributes(['class' => "fa fa-university fa-2x", 'title' => __d("Users", "User type : ") . $user->user_type->name]);
                $CssBadge = $this->templater()->formatAttributes(['class' => " stamp badge badgeAdministration"]);
            } else if ($user->user_type->name == "Company") {
                $attrsBadge = $this->templater()->formatAttributes(['class' => "fa fa-briefcase fa-2x", 'title' => __d("Users", "User type : ") . $user->user_type->name]);
                $CssBadge = $this->templater()->formatAttributes(['class' => "stamp badge badgeCompany"]);
            } else if ($user->user_type->name == "Association") {
                $attrsBadge = $this->templater()->formatAttributes(['class' => "fa fa-users fa-2x", 'title' => __d("Users", "User type : ") . $user->user_type->name]);
                $CssBadge = $this->templater()->formatAttributes(['class' => " stamp badge badgeAsso"]);
            } else {
                $attrsBadge = $this->templater()->formatAttributes(['class' => "fa fa-user fa-2x", 'title' => __d("Users", "User type : ") . $user->user_type->name]);
                $CssBadge = $this->templater()->formatAttributes(['class' => " stamp badge badgePerson"]);
            }

            $result .= $this->formatTemplate('user', [
                'UserType' => $user->user_type->name,
                'CssBadge' => $CssBadge,
                'attrsBadge' => $attrsBadge,
                'attrsColumn' => $attrsColumn,
                'attrsBlock' => $attrsBlock,
                'header' => $limit ? '<h3 class = "size-title">' : '<h2 class = "size-title">',
                'headerEnd' => $limit ? "</h3>" : "</h2>",
                'name' =>
                    strlen($user->username) > 35 ?
                        $this->Html->link($this->Text->truncate($user->username, 35, ['ellipsis' => '...', 'exact' => false]),
                            ['controller' => 'Users', 'action' => 'view', $user->id],
                            ['escape' => false, "title" => __d("Users", "User name : ") . $user->username])
                        :
                        $this->Html->link($user->username,
                            ['controller' => 'Users', 'action' => 'view', $user->id],
                            ['escape' => false,]
                        ),
//                'attrsTitle' => $this->templater()->formatAttributes(['class' => 'size-title']),
                'LinkImage' => $logo,
            ]);
        }
        return null !== $result ? $result : null;
    }

    /**
     * @param $screenshots
     * @return null
     */
    public function screenshot($screenshot)
    {
        $result = null;
        if (!empty($screenshot)) {
            if (!empty ($screenshot->url_directory) || !empty($screenshot->photo)) {
                $result .= $this->Html->tag(
                    'li',
                    $this->Html->tag(
                        'div',
                        $this->Html->link($this->Html->image(COMPTOIR_SRV_URL . $screenshot->url_directory . DS . $screenshot->photo,
                            ['alt' => __d("Screenshot name : {0}", $screenshot->photo), 'class' => 'img-responsive ']),
                            COMPTOIR_SRV_URL . $screenshot->url_directory . DS . $screenshot->photo,
                            ['escape' => false])
                        ,
                        ['class' => 'size-title screenShot']
                    )
                    ,
                    ["class" => " col-xs-12 col-sm-6 col-md-3 col-lg-3 "]
                );
            }
        }
        return null !== $result ? $result : null;

    }


    public function review($review, $limit = true)
    {
        $result = "";
        if (!empty($review)) {
            $starsElement = "";
            for ($stars = 0; $stars < $review->evaluation; $stars++) {
                $starsElement .= $this->formatTemplate('stars',
                    [
                        'attrGlyph' => $this->templater()->formatAttributes(["class" => "glyphicon glyphicon-star"]),
                        'attrStar' => $this->templater()->formatAttributes(["class" => "sr-only"]),
                        'content' => "*",
                    ]
                );
            }

            $result .= $this->formatTemplate('review', [
                'reviewLi' => $this->templater()->formatAttributes(['class' => "col-xs-11 col-sm-12 col-md-6 col-lg-6"]),
                'LinkReviewPage' => $this->Html->link("",
                    ['controller' => 'Softwares', 'action' => 'review', "?" => ["review_id" => $review->id, "software_id" => $review->software_id]],
                    ["class" => "linkReview", 'escape' => false,]
                ),
//                    $this->templater()->formatAttributes(["class" => "linkReview", 'href' => $this->request->base . "/softwares/" . $review->software_id . "/review/" . $review->id]),
                'attrsReview' => $this->templater()->formatAttributes(['class' => "blockReview"]),
                'AttrsBootstrap1' => $this->templater()->formatAttributes(['class' => "col-xs-3"]),
                'AttrsUserName' => $this->templater()->formatAttributes(['class' => "UserReview"]),
                'title' => $this->Text->truncate($review->title, 50, ['ellipsis' => '...', 'exact' => false]),
                'AttrsBootstrap2' => $this->templater()->formatAttributes(['class' => "col-xs-9"]),
                'userName' => $this->Html->link($review->user->username,
                    ['controller' => 'Users', 'action' => 'view', $review->user->id],
                    ['escape' => false, "title" => __d("Users", "User name : ") . $review->user->username]),
                'created' => $this->Time->format(strtotime($review->created),
                    [\IntlDateFormatter::SHORT, -1],
                    I18n::locale()
                ),
                'starDiv' => $this->templater()->formatAttributes(['class' => 'ratingStar']),
                'stars' => $starsElement . " " . $review->evaluation,
                'reviewComment' => ($limit == true) ? $this->Text->truncate($review->comment, 350, ['ellipsis' => '...', 'exact' => false]) : $review->comment,
            ]);
        }
        return null !== $result ? $result : null;
    }

    /**
     * Create an ordered list of items
     * @param array $items
     * @param $name
     * @param bool $limit
     * @return null
     */
    public function items(array $items, $options, $limit = true)
    {

        $result = null;
        $seeMore = null;


        $type = $options['type'];

        ($type != "screenshot" && $type != "review")
        && ($this->request->controller == "Softwares" && $this->request->action == "view")
            ? shuffle($items) : null;

        $itemsToDisplay = ($limit == true) && $this->request->controller != "Home" ? array_slice($items, 0, Configure::read("MAX_DISPLAY")) : $items;

        if (!empty($itemsToDisplay)) {
            foreach ($itemsToDisplay as $item) {
                $result .= $this->$type($item, $limit);
            }

            // Display a block with ... containing a link to the "all see" page
            count($items) > Configure::read("MAX_DISPLAY") && $this->request->action == 'view' ?
                $seeMore = $this->seeMore($items, $options) :
                null;

            $seeMore ? $result .= $seeMore : "";
        }

        return null !== $result ? $this->Html->tag('ol', $result, ['class' => 'row list-unstyled']) : null;
    }

    public function block(array $items, array $options, $limit = true)
    {

        $seeAll = "";
        $itemsDisplay = "";

        if (!empty($items)) {
            $itemsDisplay = $this->items($items, $options, $limit);

            if (count($items) > Configure::read("MAX_DISPLAY") && $this->request->action == 'view') {
                $seeAll = $this->Html->link($options["tooManyMsg"],
                    ['controller' => $this->request->controller, 'action' => $options["link"]["action"], $options["link"]["id"]],
                    ['title' => $options["titleSeeAll"]],
                    ['escape' => false]);
            }
        }

        $this->userIsInList($items,$options) ? $participateLink = $this->fallBack($options) : $participateLink = $this->participate($options);


        $header = $this->formatTemplate(
            'header',
            [
                'sectionTitle' => $options["title"],

                'indicator' => isset($options["indicator"]) ? $this->formatTemplate('indicator', [
                        "attribsIndicator" => $this->templater()->formatAttributes(["class" => "fa fa-question-circle fa indicator", "aria-hidden" => true, "aria-describedby" => $options["indicator"]["idTooltip"]]),
                        'indicatorMessage' => $options["indicator"]["indicatorMessage"],
                        'attribsToolTip' => $this->templater()->formatAttributes(["role" => "tooltip", "id" => $options["indicator"]["idTooltip"], "class" => "warning-form bg-warning"])]
                ) : null,


                    'spanAttr' => $this->templater()->formatAttributes($options),
                    'link' => $seeAll,
                    'addMore' => $participateLink,
                ]);

        $searchForm = isset($options["form"]) ? $this->formatTemplate(
            'searchForm',
            [
                'form' => $options["form"],
                'attrsform' => $this->templater()->formatAttributes(["class"=>"searchFormFilters"]),
            ]
        ): null;

        if (strpos($itemsDisplay, '<li') == false) {
            $itemsDisplay .= $this->Html->tag(
                'p',
                $options["emptyMsg"]
            );
        }
        return $header . $searchForm . $itemsDisplay;
    }

    /**
     * @param array $options
     * @return mixed
     */
    public function participate(array $options)
    {

        /**
         * Link to participate : add something
         */
//            TODO : Refaire proprement gère les action non implémentées
        if (
            (($this->request->session()->read('Auth.User.user_type') !== "Company"))
            && $this->request->controller != "Home"
        ) {
            return $this->participateActions($options);

//            TODO : Refaire proprement gère les action non implémentées

        } elseif (
            $this->request->session()->read('Auth.User.user_type') === "Company"
            && $this->request->controller != "Home"
        ) {
//            TODO : Refaire proprement gère les action non implémentées
            return
                isset($options['linkParticipate']) && $options['linkParticipate']['action'] !== "addReview"
                && $options['linkParticipate']['action'] !== "addUserOf" ?

                    $this->Html->link($options["linkParticipate"]["text"],
                        ['controller' => $this->request->controller, 'action' => $options['linkParticipate']["action"], $options['linkParticipate']["id"]],
                        ['class' => 'btn btn-default addmore',],
                        ['escape' => false]) :

                    null;
        }
        return null;
    }

    /**
     * Display the participate link for a user
     * @param array $options
     * @return mixed
     */
    private function participateActions(array $options)
    {
        //TODO : ACL for v2
        $options["titleAddMore"] != "" ? $title = ['title' => $options["titleAddMore"]] : $title = null; //for a11y
        if ($this->request->session()->read('Auth.User.user_type') === "Administration" || !$this->request->session()->check('Auth.User')){
            return
                isset($options['linkParticipate']) ?

                    $this->Html->link($options["linkParticipate"]["text"],
                        ['controller' => $this->request->controller, 'action' => $options['linkParticipate']["action"], $options['linkParticipate']["id"]],
                        ['class' => 'btn btn-default addmore', "title" => $title],
                        ['escape' => false]) :

                    null;
        }else{
            return
                (isset($options['linkParticipate']) && $options['linkParticipate']['action'] !== "addReview" ) ?

                    $this->Html->link($options["linkParticipate"]["text"],
                        ['controller' => $this->request->controller, 'action' => $options['linkParticipate']["action"], $options['linkParticipate']["id"]],
                        ['class' => 'btn btn-default addmore', "title" => $title],
                        ['escape' => false]) :

                    null;
        }
    }

    /**
     * @param array $options
     */
    public function fallBack(array $options)
    {

        return
            isset($options['linkFallBack']) ?

                $this->Html->link($options["linkFallBack"]["text"],
                    ['controller' => $this->request->controller, 'action' => $options['linkFallBack']["action"], $options['linkFallBack']["id"]],
                    ['class' => 'btn btn-default removeOne', 'title' => $options["titleFallBack"]],
                    ['escape' => false]) :

                null;
    }

    private function seeMore($items, $options)
    {
        return $this->formatTemplate(
            'seeMore',
            [
                'attrsColumn' => $attrsBlock = $this->templater()->formatAttributes(['class' => 'col-xs-12 col-sm-6 col-md-3 col-lg-3']),
                'attribs' => $this->templater()->formatAttributes(
                    ["class" =>
                        $options["type"] .
                        "-unit-home unit-see-more "
                        . ($options["type"] == "review" ? "blockReview" : "backgroundUnit")
                    ]),
                'link' => $this->Html->link("",
                    ['controller' => $this->request->controller, 'action' => $options["link"]["action"], $options["link"]["id"]],
                    ['title' => $options["titleSeeAll"], 'class' => 'fa fa-chevron-right'],
                    ['escape' => false])
            ]);
    }

    /**
     * retunr TRUE if the current user is in the list, FALSE OTHERWISE
     */
    private function userIsInList (array $items,$options=[]) {

        if (isset($options["type"]) && $options["type"]=="user"){
            foreach ($items as $item){
                if ( (Hash::contains((array)$item, ['id'=>$this->request->session()->read('Auth.User.id')])) ){
                    return true;
                }
            }
        }else{
            foreach ($items as $item){
                if ((Hash::contains((array)$item, ['user_id'=>$this->request->session()->read('Auth.User.id')]))){
                    return true;
                }
            }
        }
        return false;

    }
}
