<?php

namespace App\View\Helper;

use Cake\Core\Configure;
use Cake\View\Helper;
use Cake\View\StringTemplateTrait;

class StampHelper extends Helper
{
    use StringTemplateTrait;

    protected $_defaultConfig = [
        'templates' => [
            'score_default' => '<{{tag}}{{attrs}}><abbr {{title}}>{{score}}</abbr></{{tag}}>',
            'score_raw' => '{{score}}',
            'prefix' => '<{{tag}} {{attrs}}>{{text}}</{{tag}}>',
            'prefix_raw' => '{{text}}',
            'suffix' => '<{{tag}} {{attrs}}>{{text}}</{{tag}}>',
            'suffix_raw' => '{{text}}',
        ]
    ];
    public $helpers = ['Html'];

    /**
     * Function : className
     * Takes an array of thresholds and a stamp and finds the correct html class based on the stamp.
     * Each 'key' of thresholds is a threshold and its 'value' is the class.
     * @param $stamp
     * @param array $thresholds
     * @return int|string
     */
    public function className($stamp, array $thresholds = [])
    {
        // FIXME: $thresholds vide
        if ($stamp !== null) {
            $count = count($thresholds);
            asort($thresholds);
            $stamp = round($stamp, 0);
            $low = 0;
            $i = 1;

            foreach ($thresholds as $class => $high) {
                if ($stamp >= $low && ($stamp < $high || $i === $count && $stamp == $high)) {
                    return $class;
                }
                $low = $high;
                $i++;
            }
        }
        return 'stamp_NA';
    }

    /** Function :  prefix
     * Creates a prefix in html form
     * @param array $config
     * @return string
     */
    public function prefix(array $config = [])
    {
        $options = [];
        // By default the template is set to 'score_default'. 'raw' is another template currently available.
        if (!array_key_exists("template", $config))
            $config['template'] = 'prefix';

        // By default the tag is set to 'span'.
        if (!array_key_exists("tag", $config))
            $config['tag'] = 'span';

        // By default the text is set to null.
        if (!array_key_exists("text", $config))
            $config['text'] = '';

        // By default the class is set to depend on the score.
        if (!array_key_exists("class", $config))
            $options = ["class" => ""];
        else
            $options['class'] = $config['class'];

        return $this->formatTemplate(
            $config['template'],
            [
                'attrs' => $this->templater()->formatAttributes($options),
                'tag' => $config['tag'],
                'text' => $config['text'],
            ]
        );
    }

    /** Function : note
     * @param int $stamp : takes the stamp of the software and rounds it up.
     * @param array $config : takes all the config options for the badge. Options are the template, the tag, and the class
     * @return string
     */
    public function note($stamp, array $config = [])
    {
        $stamp = $stamp !== null ? round($stamp, 0) : null;
        $options = [];
        $options2 = [];

        $config += [
            'thresholds' => (array)Configure::read('Stamps')
        ];

        // By default the template is set to 'stamp_default'. 'raw' is another template currently available.
        if (!array_key_exists("template", $config))
            $config['template'] = 'unstamped';

        // By default the tag is set to 'span'.
        if (!array_key_exists("tag", $config))
            $config['tag'] = 'span';

        // By default the class is set to depend on the stamp.
        if (!array_key_exists("class", $config)) {
            $options = ["class" => "stamp " . $this->className($stamp, $config['thresholds'])];
        }else {
            $options['class'] = $config['class'];
        }

        // By default the title is set in english.
        if (!array_key_exists("title", $config)) {
            $options2['title'] = "This stamp is the mark of the software";
        }else {
            $options2['title'] = $config['title'];
        }

        return $this->formatTemplate(
            $config['template'],
            [
                'attrs' => $this->templater()->formatAttributes($options),
                'stamp' => $stamp == null ? "NA" : $stamp,
                'tag' => $config['tag'],
                'title' => $this->templater()->formatAttributes($options2),
            ]
        );
    }

    public function suffix(array $config = [])
    {
        $options = [];
        // By default the template is set to 'score_default'. 'raw' is another template currently available.
        if (!array_key_exists("template", $config))
            $config['template'] = 'suffix';

        // By default the tag is set to 'span'.
        if (!array_key_exists("tag", $config))
            $config['tag'] = 'span';

        // By default the text is set to null.
        if (!array_key_exists("text", $config))
            $config['text'] = '';

        // By default the class is set to depend on the score.
        if (!array_key_exists("class", $config))
            $options = ["class" => ""];
        else
            $options['class'] = $config['class'];

        return $this->formatTemplate(
            $config['template'],
            [
                'attrs' => $this->templater()->formatAttributes($options),
                'tag' => $config['tag'],
                'text' => $config['text'],
            ]
        );
    }
}