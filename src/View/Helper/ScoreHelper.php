<?php

namespace App\View\Helper;

use Cake\Core\Configure;
use Cake\View\Helper;
use Cake\View\StringTemplateTrait;

class ScoreHelper extends Helper
{
    use StringTemplateTrait;

    protected $_defaultConfig = [
        'templates' => [
            'score_default' => '<{{tag}}{{attrs}}><abbr {{title}}>{{score}}</abbr></{{tag}}>',
            'score_raw' => '{{score}}',
            'prefix' => '<{{tag}} {{attrs}}>{{text}}</{{tag}}>',
            'prefix_raw' => '{{text}}',
            'suffix' => '<{{tag}} {{attrs}}>{{text}}</{{tag}}>',
            'suffix_raw' => '{{text}}',
        ]
    ];
    public $helpers = ['Html'];

    /**
     * Function : className
     * Takes an array of thresholds and a score and finds the correct html class based on the score.
     * Each 'key' of thresholds is a threshold and its 'value' is the class.
     * @param $score
     * @param array $thresholds
     * @return int|string
     */
    public function className($score, array $thresholds = [])
    {
        // FIXME: $thresholds vide
        if ($score !== null) {
            $count = count($thresholds);
            asort($thresholds);
            $score = round($score, 0);
            $low = 0;
            $i = 1;

            foreach ($thresholds as $class => $high) {
                if ($score >= $low && ($score < $high || $i === $count && $score == $high)) {
                    return $class;
                }
                $low = $high;
                $i++;
            }
        }
        return 'score_NA';
    }

    /** Function :  prefix
     * Creates a prefix in html form
     * @param array $config
     * @return string
     */
    public function prefix(array $config = [])
    {
        $options = [];
        // By default the template is set to 'score_default'. 'raw' is another template currently available.
        if (!array_key_exists("template", $config))
            $config['template'] = 'prefix';

        // By default the tag is set to 'span'.
        if (!array_key_exists("tag", $config))
            $config['tag'] = 'span';

        // By default the text is set to null.
        if (!array_key_exists("text", $config))
            $config['text'] = '';

        // By default the class is set to depend on the score.
        if (!array_key_exists("class", $config))
            $options = ["class" => ""];
        else
            $options['class'] = $config['class'];

        return $this->formatTemplate(
            $config['template'],
            [
                'attrs' => $this->templater()->formatAttributes($options),
                'tag' => $config['tag'],
                'text' => $config['text'],
            ]
        );
    }

    /** Function : note
     * @param int $score : takes the score of the software and rounds it up.
     * @param array $config : takes all the config options for the badge. Options are the template, the tag, and the class
     * @return string
     */
    public function note($score, array $config = [])
    {
        $score = $score !== null ? round($score, 0) : null;
        $options = [];
        $options2 = [];

        $config += [
            'thresholds' => (array)Configure::read('Softwares.scores')
        ];

        // By default the template is set to 'score_default'. 'raw' is another template currently available.
        if (!array_key_exists("template", $config))
            $config['template'] = 'score_default';

        // By default the tag is set to 'span'.
        if (!array_key_exists("tag", $config))
            $config['tag'] = 'span';

        // By default the class is set to depend on the score.
        if (!array_key_exists("class", $config)) {
            $options = ["class" => "score " . $this->className($score, $config['thresholds'])];
        }else {
            $options['class'] = $config['class'] . $this->className($score, $config['thresholds']);
        }

        // By default the title is set in english.
        if (!array_key_exists("title", $config)) {
            $options2['title'] = "This score is the mark of the software";
        }else {
            $options2['title'] = $config['title'];
        }

        return $this->formatTemplate(
            $config['template'],
            [
                'attrs' => $this->templater()->formatAttributes($options),
                'score' => $score == null ? "NA" : $score,
                'tag' => $config['tag'],
                'title' => $this->templater()->formatAttributes($options2),
            ]
        );
    }

    public function suffix(array $config = [])
    {
        $options = [];
        // By default the template is set to 'score_default'. 'raw' is another template currently available.
        if (!array_key_exists("template", $config))
            $config['template'] = 'suffix';

        // By default the tag is set to 'span'.
        if (!array_key_exists("tag", $config))
            $config['tag'] = 'span';

        // By default the text is set to null.
        if (!array_key_exists("text", $config))
            $config['text'] = '';

        // By default the class is set to depend on the score.
        if (!array_key_exists("class", $config))
            $options = ["class" => ""];
        else
            $options['class'] = $config['class'];

        return $this->formatTemplate(
            $config['template'],
            [
                'attrs' => $this->templater()->formatAttributes($options),
                'tag' => $config['tag'],
                'text' => $config['text'],
            ]
        );
    }
}