# COMPTOIR du LIBRE - Web (comptoir-web)

Comptoir-Web is the web application of Comptoir du Libre. It is the user interface of the application, and speaks with its engine counterpart: Comptoir-Srv.

All documentation is in the Documentation/ directory.